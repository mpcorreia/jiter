package jiter;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;

import jiter.misc.Config;
import jiter.misc.Log;
import jiter.misc.Utils;

import org.jgroups.Address;
import org.jgroups.View;
import org.jgroups.ViewId;

import jiter.strategies.DirectStrategy;

/**
 * This class represents a Jiter Node - a special gateway that forwards the
 * traffic towards the final destination.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @author Rui Silva
 * @version 1.0
 * 
 */
public class JiterNode { 
	
	/** File with the default socket configuration. */
    private final String defaultSocketConfig = "jgroup_channelconfig.xml";
    
    private final String testSocketConfig = "tests_channelconfig.xml";

    /** The id of the Jitter Node. */
    private String _id;

    /** Set of addresses of the Jiter Node. */
    private Set<Address> _remoteSocketAddresses;
    
    /** Set of Local Sockets. */
    private Map<String, JiterSocket> _localSockets;

    /** DC Matrix of the Jiter Node. */
    private DirectChannelMatrix _dcMatrix;
    
    /** OC Matrix of the Jiter Nod. */
    private OverlayChannelMatrix _ocMatrix;

    /** Current View ID - needed to order the views of the channels. */
    private ViewId _currentViewId;
    
    private ViewId _currentTestViewId;

    private List<IJiterNodeObserver> _registeredObservers;

    private DirectStrategy _directStrategy;
    
    private Log _log;
    
    private JiterSocket _testJiterSocket;
    
    private Map<String, Address> _remoteTestSocketAddresses;

    /**
     * Creates an instance of a Jiter Node.
     */
    public JiterNode(String[] args) throws Exception {
        _id = UUID.randomUUID().toString();
        _remoteSocketAddresses = new TreeSet<Address>();
        _localSockets = new HashMap<String, JiterSocket>();
        _dcMatrix = new DirectChannelMatrix(this);
        _ocMatrix = new OverlayChannelMatrix(this);
        _currentViewId = null;
        _currentTestViewId = null;
        _registeredObservers = new LinkedList<IJiterNodeObserver>();
        Config.initialize(this).parseArguments(args);
        Config.getInstance().getSendStrategy().initialize();
        _directStrategy = new DirectStrategy(this);
        _directStrategy.initialize();
        _log = Log.getInstance();
        _testJiterSocket = null;
        _remoteTestSocketAddresses = new TreeMap<String, Address>();
    }

    /**
     * Scans local network interfaces and automatically creates sockets for them.
     * It also creates the test socket.
     */
    public void scanAndCreateSockets() throws SocketException {
        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
        Config config = Config.getInstance();
        int i = 0;
        boolean socketTestCreated = false;

        for (NetworkInterface netint : Collections.list(nets)) {
            try {
                if (config.isLocalhostOnly()) {
                    // Ignore non-loopback interfaces (localhost)
                    if (!netint.isLoopback()) continue;
                } else {
                    // Ignore loopback interfaces (localhost)
                    if (netint.isLoopback()) continue;
                }

                Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
                for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                    if (inetAddress instanceof Inet4Address &&
                        !config.getIgnoredInterfaces().contains(inetAddress)) {
                        createLocalSocket("isp" + i, inetAddress);
                        if (socketTestCreated == false) {
                        	_testJiterSocket = new JiterSocket(this, "isp" + i, testSocketConfig, inetAddress);
                            if (config.isResolvePublicIps()) {
                                _testJiterSocket.discoverPublicIp();
                            }
                        	socketTestCreated = true;
                        }
                        i++;
                        break;
                    }
                }
            } catch (Exception e) {
            	_log.e(e.getMessage());
            	_log.e(Utils.getStackTraceString(e));
            }
        }
    }

    /**
     * Creates a Local Socket for a given ISP.
     * 
     * @param isp The ISP for which the socket is created.
     * @throws JiterSocketException
     */
    public void createLocalSocket(String isp) throws JiterSocketException {
        createLocalSocket(isp, defaultSocketConfig, null);
    }

    public void createLocalSocket(String isp, InetAddress bindAddress) throws JiterSocketException {
        createLocalSocket(isp, defaultSocketConfig, bindAddress);
    }

    /**
     * Creates a Local Socket for a given ISP, according to a certain custom configuration.
     * 
     * @param isp The ISP for which the socket is created.
     * @param customConfig File of the custom configuration.
     * @throws JiterSocketException
     */
    public void createLocalSocket(String isp, String customConfig, InetAddress bindAddress) throws JiterSocketException {
        JiterSocket socket = new JiterSocket(this, isp, customConfig, bindAddress);
        _localSockets.put(isp, socket);

        if (Config.getInstance().isResolvePublicIps()) {
            socket.discoverPublicIp();
        }
    }

    /**
     * Returns a local socket of the Jiter Node.
     * In order to achieve load balance, each time this method is invoked,
     * a different local socket might be returned.
     * 
     * @return A local socket.
     */
    public JiterSocket getLocalSocket() {
        try {
            return _localSockets.values().iterator().next();
        } catch (Exception e) {
        	_log.e(e.getMessage());
        	_log.e(Utils.getStackTraceString(e));
        	return null;
        }
    }

    /**
     * Returns all the local sockets related to a given ISP.
     * 
     * @param isp The ISP related to the local sockets we want to get.
     * @return All the local sockets related to a given ISP
     */
    public JiterSocket getLocalSocket(String isp) {
        return _localSockets.get(isp);
    }
    
    public JiterSocket getLocalSocket(Address address) {
        for (JiterSocket localSocket : _localSockets.values()) {
            Address localSocketAddress = localSocket.getAddress();
            if (localSocketAddress == null) {
                continue;
            }
            else if (localSocketAddress.equals(address)) {
                return localSocket;
            } 
        }

        return null;
    }

    /**
     * Returns all the local sockets.
     * 
     * @return All the local sockets.
     */
    public List<JiterSocket> getLocalSockets() {
        return new ArrayList<JiterSocket>(_localSockets.values());
    }
    
    public JiterSocket getTestJiterSocket() {
    	return _testJiterSocket;
    }

    /**
	 * Returns the Id of the Jiter node.
	 * @return the Id of the Jiter node.
	 */
	public String getId() {
	    return _id;
	}

	/**
	 * Starts the execution of the node.
	 * 
	 * @throws JiterSocketException
	 */
	public void start() throws JiterSocketException {
        if (_localSockets.size() == 0) {
            return;
        }

        for (JiterSocket socket : _localSockets.values()) {
            socket.start();
        }
        
        if (_testJiterSocket != null) {
            _testJiterSocket.start(true);
        }

        viewAccepted(getLocalSocket().getView());

        if (_testJiterSocket != null) {
            viewAccepted(_testJiterSocket.getView());
        }
    }

	/**
	 * Stops the execution of the node.
	 * A node that was stopped, can be started again later.
	 * 
	 * @throws JiterSocketException
	 */
    public void stop() throws JiterSocketException {
        for (JiterSocket socket : _localSockets.values()) {
            socket.stop();
        }
        _testJiterSocket.stop();
    }

    /**
     * Closes the execution of the node.
     * The difference between the stop and the close of a node is that the last one
     * will destroy the channel and the associated resources.
     * 
     * @throws JiterSocketException
     */
    public void close() throws JiterSocketException {
        for (JiterSocket socket : _localSockets.values()) {
            socket.close();
        }
        
        _testJiterSocket.close();
    }

    /**
     * Verifies if a given Address is registered.
     * 
     * @param address The Address to verify
     * @return True if the Address is registered and false otherwise.
     */
    public boolean isRegisteredRemoteAddress(Address address) {
        return _remoteSocketAddresses.contains(address);
    }

    /**
     * Returns the remote socket Addresses known.
     * 
     * @return The remote socket Addresses known.
     */
    public Collection<Address> getKnownRemoteSocketAddresses() {
        return new TreeSet<Address>(_remoteSocketAddresses);
    }

    /**
     * Verifies if a given Address is related to a local socket.
     * 
     * @param nodeAddress The Address to verify.
     * @return True if the Address is related to a local socket.
     */
    public boolean isLocalSocketAddress(Address nodeAddress) {
        for (JiterSocket localSocket : _localSockets.values()) {
            Address localSocketAddress = localSocket.getAddress();
            if (localSocketAddress == null) {
                continue;
            }
            else if (localSocketAddress.equals(nodeAddress)) {
                return true;
            } 
        }

        return false;
    }
    
    public boolean isTestSocketAddress(Address address) {
    	return _testJiterSocket.getAddress().equals(address);
    }

    /**
     * Given a name, returns the corresponding Address.
     * 
     * @param name Name of the Address.
     * @return The Address.
     */
    public Address getAddressFromName(String name) {
        if (name == null) {
            return null;
        }

        for (Address remoteAddress : _remoteSocketAddresses) {
            if (Utils.addressToLogicalName(remoteAddress).equals(name)) {
                return remoteAddress;
            }
        }

        for (JiterSocket localSocket : _localSockets.values()) {
            Address address = localSocket.getAddress();
            if (Utils.addressToLogicalName(address).equals(name)) {
                return address;
            }
        }
        
        for (Address address : _remoteTestSocketAddresses.values()) {
        	if (Utils.addressToLogicalName(address).equals(name)) {
        		return address;
        	}
        }
        
        if (Utils.addressToLogicalName(_testJiterSocket.getAddress()).equals(name)) {
        	return _testJiterSocket.getAddress();
        }

        return null;
    }
    
    public String getLogicalNameFromNodeId(String nodeId) {
        if (nodeId == null) {
            return null;
        }

        for (Address remoteAddress : _remoteSocketAddresses) {
            if (Utils.addressToNodeId(remoteAddress).equals(nodeId)) {
                return Utils.addressToLogicalName(remoteAddress);
            }
        }

        for (JiterSocket localSocket : _localSockets.values()) {
            Address address = localSocket.getAddress();
            if (Utils.addressToNodeId(address).equals(nodeId)) {
                return Utils.addressToLogicalName(address);
            }
        }
        
        for (Address address : _remoteTestSocketAddresses.values()) {
        	if (Utils.addressToNodeId(address).equals(nodeId)) {
        		return Utils.addressToLogicalName(address);
        	}
        }
        
        if (Utils.addressToNodeId(_testJiterSocket.getAddress()).equals(nodeId)) {
        	return Utils.addressToLogicalName(_testJiterSocket.getAddress());
        }

        return null;
    }
    

	public JiterDirectChannel getJiterDirectChannel(String sourceName,
			String destinationName) {
		Address sourceAddress = getAddressFromName(sourceName);
		Address destinationAddress = getAddressFromName(destinationName);
		
		return _dcMatrix.get(sourceAddress, destinationAddress);
	}

    /**
     * Returns the DC Matrix of the jiter node.
     * 
     * @return The DC Matrix of the jiter node.
     */
    public DirectChannelMatrix getDirectChannelMatrix() {
        return _dcMatrix;
    }

    /**
     * Returns the OC Matrix of the jiter node.
     * 
     * @return The OC Matrix of the jiter node.
     */
    public OverlayChannelMatrix getOverlayChannelMatrix() {
        return _ocMatrix;
    }

    /**
     * Takes in account a new View for this jiter node.
     * Updates the remote socket addresses and the DC Matrix of the jiter node
     * with the non local sockets of the View. It removes the old remote sockets
     * as well.
     * 
     * @param newView
     */
    public synchronized void viewAccepted(View newView) {

        // Adding the new remote socket addresses and update the DC Matrix, or
        // the map of test addresses..
        if (_testJiterSocket != null && newView.containsMember(_testJiterSocket.getAddress())) {
        	if (newView.getViewId().equals(_currentTestViewId)) {
        		return;
        	}
        	
        	for (Address socketAddress : newView) {
                if (_testJiterSocket.getAddress().equals(socketAddress)) {
                    continue;
                }
                
            	_log.d("JiterNode: Found new test jiter socket.");
            	
	            if (!_remoteTestSocketAddresses.containsValue(socketAddress)) {
	                _remoteTestSocketAddresses.put(Utils.addressToNodeId(socketAddress), socketAddress);
	            }
        	}
        	
            // Removing old remote addresses.
            Iterator<Address> it = _remoteTestSocketAddresses.values().iterator();
        	
            while (it.hasNext()) {
                Address socketAddress = it.next();

                if (!newView.containsMember(socketAddress)) {
                    it.remove();
                }
            }

            _currentTestViewId = newView.getViewId();
        	
        	
        } else {
        	if (newView.getViewId().equals(_currentViewId)) {
                return;
            }
        	
            // Adding the new remote socket addresses and update the DC Matrix.
            for (Address socketAddress : newView) {
                if (isLocalSocketAddress(socketAddress)) {
                    continue;
                }
                
                //if (!_jiterNodes.contains(Utils.addressToNodeId(socketAddress))) {
                    //_jiterNodes.add(Utils.addressToNodeId(socketAddress));
                //}
                
                if (!_remoteSocketAddresses.contains(socketAddress)) {
                    _remoteSocketAddresses.add(socketAddress);

                    for (JiterSocket localSocket : _localSockets.values()) {
                        _dcMatrix.addLocalDirectChannel(new JiterDirectChannel(localSocket, socketAddress));
                    }
                } 
            }

            // Removing old remote addresses.
            Iterator<Address> it = _remoteSocketAddresses.iterator();

            while (it.hasNext()) {
                Address socketAddress = it.next();

                if (!newView.containsMember(socketAddress)) {
                    _dcMatrix.removeChannelsWithSocketAddress(socketAddress);
                    it.remove();
                }
            }

            _currentViewId = newView.getViewId();
        }
        
        for (IJiterNodeObserver observer : _registeredObservers) {
            observer.onViewChanged(newView);
        }
    }

    public void directSend(JiterMessage message, String destinationNodeId) throws JiterSocketException {
        _directStrategy.sendTestMessage(message, _remoteTestSocketAddresses.get(destinationNodeId));
    }

    public void send(JiterMessage message, float deadline, String destinationNodeId) {
    	Config.getInstance().getSendStrategy().send(message, deadline, destinationNodeId);
    }
    
    public void sendTestMessage(JiterMessage message, String destinationNodeId) throws JiterSocketException {
    	directSend(message, destinationNodeId);
    	//_testJiterSocket.sendMessage(_remoteTestSocketAddresses.get(destinationNodeId), message);
    }

    public void notifySendSuccessful(long messageId) {
        for (IJiterNodeObserver observer : _registeredObservers) {
            observer.onSendSuccessful(messageId);
        }
    }

    public void notifySendFailed(long messageId) {
        for (IJiterNodeObserver observer : _registeredObservers) {
            observer.onSendFailed(messageId);
        }
    }

    public void notifyDataReceived(JiterMessage message) {
        for (IJiterNodeObserver observer : _registeredObservers) {
            observer.onDataReceived(message);
        }
    }

    public void registerObserver(IJiterNodeObserver observer) {
        _registeredObservers.add(observer);
    }

    /**
     * Returns a string representation of the Jiter Node.
     * 
     * @return A string representation of the Jiter Node.
     */
    public String toString() {
        String string = getId() + " {\n";

        for (JiterSocket socket : _localSockets.values()) {
            string += "\t" + socket.toString() + "\n";
        }

        string += "}";

        return string;
    }
    
    /**
     * This class represents an exception in a Jiter Node.
     * 
	 * @author Alexandre Fonseca
	 * @author Pedro Marques da Luz
	 * @author Rui Silva
	 * @version 1.0
     */
    public static class JiterNodeException extends Exception {
        static final long serialVersionUID = 1;

        /**
         * Creates an instance of a Jiter Node Exception.
         * 
         * @param cause The cause of the Exception.
         */
        public JiterNodeException(Throwable cause) {
            super(cause);
        }
    }

    public interface IJiterNodeObserver {
        public void onSendSuccessful(long messageId);
        public void onSendFailed(long messageId);
        public void onViewChanged(View view);
        public void onDataReceived(JiterMessage message);
    }
}
