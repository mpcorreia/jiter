package jiter;
/**
 * This class represents an Exception in a Jiter Socket.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @author Rui Silva
 * @version 1.0
 *
 */
public class JiterSocketException extends Exception {
    static final long serialVersionUID = 1;

    /**
     * Creates an instance of the Exception.
     * 
     * @param cause The cause of the Exception.
     */
    public JiterSocketException(Throwable cause) {
        super(cause);
    }

    public JiterSocketException(String cause) {
        super(cause);
    }
}
