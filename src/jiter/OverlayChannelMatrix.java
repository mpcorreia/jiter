package jiter;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;
import java.util.Collections;
import java.util.Comparator;

import java.util.LinkedList;

/**
 * This class represents a matrix of overlay channels (OC) that is built from a 
 * DC matrix.For each know node we keep track of all possible channels 
 * (indirect and direct) with that node as destination.
 * This class implements Observer as it will check for updates on every modification
 * on the DC matrix.
 *
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @version 1.0
 * @see DirectChannelMatrix
 * @see IjitterChannel
 */
public class OverlayChannelMatrix implements Observer {
    private Map<String, List<IJiterChannel>> _ocMap;
    private JiterNode _node;
    private DirectChannelMatrix _dcMatrix;
    
	/**
	 * Create an OC matrix, obtain a DC matrix from a given node
	 * and assigns itself as observer to the DC matrix.
	 * 
	 * @param node JiterNode responsible for this OC matrix.
	 */
    public OverlayChannelMatrix(JiterNode node) {
        _ocMap = new TreeMap<String, List<IJiterChannel>>();
        _node = node;
        _dcMatrix = node.getDirectChannelMatrix();
        _dcMatrix.addObserver(this);
    }

	/**
	 * Implementation of update method required to use Observer design pattern.
	 * This method is called every time there is a modification in the DC matrix
	 * attached to this OC matrix 
	 *
	 * @param o The observable object.
	 * @param arg Arguments passed to the notifyObservers  method.
	 * @see java.util.Observer;
	 */
    public void update(Observable o, Object arg) {
        synchronized(_ocMap) {
            _ocMap.clear();

            for (JiterDirectChannel channelX : _dcMatrix.getChannelsWithSourceNode(_node.getId())) {
                String destinationNodeId = channelX.getDestinationNodeId();
                List<IJiterChannel> channels = getChannelListOrInitialize(destinationNodeId);

                // Add direct channel
                channels.add(channelX);

                for (JiterDirectChannel channelY : _dcMatrix.getChannelsWithSourceNode(destinationNodeId)) {
                    JiterIndirectChannel indirectChannel = new JiterIndirectChannel(channelX, channelY);
                    channels = getChannelListOrInitialize(indirectChannel.getDestinationNodeId());
                    channels.add(indirectChannel);
                }
            }
        }
    }

    /**
     * If there is already a channel for the Address of the destination,
     * returns those channels. If not, initializes the list of channels 
     * and returns it.
     * 
     * @param nodeId The destination node.
     * @return List of the channels whose destination is the given Address.
     */
    private List<IJiterChannel> getChannelListOrInitialize(String destinationNodeId) {
        synchronized(_ocMap) {
            List<IJiterChannel> channels = _ocMap.get(destinationNodeId);

            if (channels == null) {
                channels = new LinkedList<IJiterChannel>();
                _ocMap.put(destinationNodeId, channels);
            }

            return channels;
        }
    }
    
	 /**
	 * Obtains a list of jiter channels with a given destination node id
	 *
	 * @param destinationNodeId Destination node id.
	 * @return List of jiter channels.
	 */
    public List<IJiterChannel> getChannelsToDestination(String destinationNodeId) {
        List<IJiterChannel> channelList;
        synchronized(_ocMap) {
            channelList = _ocMap.get(destinationNodeId);
        }

        if (channelList == null) {
            return null;
        }

        Collections.sort(channelList, new JiterChannelTxtComparator());
        return Collections.unmodifiableList(channelList);
    }

	/**
	 * Obtains a list of jiter channels with given destination where estimated TXT is lower
	 * than especified.
	 *
	 * @param destinationNodeId Destination node id
	 * @param maxTxt Maximum txt required
	 * @return List of jiter channels that meet requirements
	 */
    public List<IJiterChannel> getChannelsToDestination(String destinationNodeId, float maxTxt) {
        List<IJiterChannel> channelList = getChannelsToDestination(destinationNodeId);

        for (int i = 0; i < channelList.size(); i++) {
            IJiterChannel channel = channelList.get(i);

            if (channel.getTxt() > maxTxt) {
                return channelList.subList(0, i);
            }
        }

        return channelList;
    }
    
	/**
	 * Class that implements a comparator for txt of jiter channels
	 *
	 * @see java.util.Comparator;
	 */
    public class JiterChannelTxtComparator implements Comparator<IJiterChannel> {
		/**
		 * Compares txt of two given channels
		 *
		 * @param o1 Jiter channel
		 * @param o2 Jiter channel
		 * @return Result of comparation
		 */
        public int compare(IJiterChannel o1, IJiterChannel o2) {
            if (o1.getTxt() == o2.getTxt()) {
            	return 0;
            } else if (o1.getTxt() > o2.getTxt()) {
            	return 1;
            } else return -1;
        }
    }
}
