package jiter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import jiter.handlers.IMessageHandler;
import jiter.misc.Config;
import jiter.misc.Log;
import jiter.misc.Utils;

import org.jgroups.Address;
import org.jgroups.Event;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.PhysicalAddress;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.jgroups.protocols.TP;
import org.jgroups.stack.AddressGenerator;
import org.jgroups.stack.IpAddress;
import org.jgroups.util.PayloadUUID;

/**
 * This class represents a Jiter Socket.
 * A Jiter Socket is associated with an ISP and a channel.
 * It allows to receive, send and forward Jiter Messages.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @author Rui Silva
 * @version 1.0
 * 
 */
public class JiterSocket extends ReceiverAdapter {
	
    /** The Jiter Node related to the Jiter Socket. */
    private JiterNode _node;

    /** The Channel to which the Jiter Socket is associated. */
    private JChannel _channel;
    
    /** The name of the ISP to which the Jiter Socket is associated. */
    private String _isp;
    
    /** Flag that indicates if the Socket is running or not. */
    private boolean _running;

    private InetAddress _bindAddress;

    public JiterSocket(JiterNode node, String isp, String config) throws JiterSocketException {
        this(node, isp, config, null);
    }

    /**
     * Creates an instance of a Jiter Socket.
     * 
     * @param node The Jiter Node related to the Jiter Socket.
     * @param isp The name of the ISP to which the Jiter Socket is associated.
     * @param config The name of the file with the configuration of the Jiter Socket
     * @param bindAddr The address to which to bind this socket (null if none specified)
     * @throws JiterSocketException
     */
    public JiterSocket(JiterNode node, String isp, String config, InetAddress bindAddr) throws JiterSocketException {
        try {
            _node = node;
            _channel = new JChannel(new File(config));
            if (bindAddr != null) {
                _channel.getProtocolStack().getTransport().setBindAddress(bindAddr);
            }
            _channel.setAddressGenerator(new AddressGenerator() {
                public Address generateAddress() {
                    PayloadUUID address = PayloadUUID.randomUUID(_node.getId());
                    return address;
                }
            });
            _bindAddress = _channel.getProtocolStack().getTransport().getBindAddress();
            _isp = isp;
            _running = false;
        } catch (Exception e) {
            throw new JiterSocketException(e);
        }
    }
    
    public InetAddress getBindAddress() {
        return _bindAddress;
    }

    public void discoverPublicIp() {
        try {
            // Create a URL object
            URL url = new URL("http://icanhazip.com/");
 
            // Read all of the text returned by the HTTP server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
 
            String htmlText;
 
            while ((htmlText = in.readLine()) != null) {
                if (htmlText.matches("\\d+\\.\\d+\\.\\d+\\.\\d+")) {
                    Log.getInstance().d("Found public IP (" + htmlText + ") of socket bound to " + _bindAddress.getHostName());
                    TP tp = _channel.getProtocolStack().getTransport();
                    Class tpClass = tp.getClass();
                    Field externalAddrField = Utils.getField(tpClass, "external_addr");
                    externalAddrField.setAccessible(true);
                    externalAddrField.set(tp, InetAddress.getByName(htmlText));
                }
            }

            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts the execution of the Jiter Socket, connecting it to
     * the global group used.
     * 
     * @throws JiterSocketException
     */
    public void start() throws JiterSocketException {
    	start(Config.getInstance().getGlobalCommunicationGroup());
    }
    
    public void start(String group) throws JiterSocketException {
        if (!_running) {
            try {
                _channel.connect(group);
                _channel.setReceiver(this);
                _running = true;
            } catch (Exception e) {
                throw new JiterSocketException(e);
            }
        }
    }
    
    public void start(boolean test) throws JiterSocketException {
    	if (test) {
    		start(Config.getInstance().getTestCommunicationGroup());
    	} else {
    		start(Config.getInstance().getGlobalCommunicationGroup());
    	}
    }

    /**
     * Stops the execution of the Jiter Socket, disconnecting it from
     * the global group used. A stopped Jiter Socket can be started again.
     * 
     * @throws JiterSocketException
     */
    public void stop() throws JiterSocketException {
        if (_running) {
            try {
                _channel.disconnect();
                _channel.setReceiver(null);
            } catch (Exception e) {
                throw new JiterSocketException(e);
            }
        }
    }

    /**
     * Closes the jiter Socket, destroying the channel and associated
     * resources. 
     * 
     * @throws JiterSocketException
     */
    public void close() throws JiterSocketException {
        stop();
        _channel.close();
    }

    /**
     * Returns the Jiter Node related to the Jiter Socket.
     * 
     * @return The Jiter Node related to the Jiter Socket.
     */
    public JiterNode getNode() {
        return _node;
    }

    /**
     * Returns the Address of the Jiter Socket.
     * 
     * @return The Address of the Jiter Socket.
     */
    public Address getAddress() {
        return _channel.getAddress();
    }

    /**
     * Returns the IpAddress of the Jiter Socket.
     * 
     * @param address The Address of the Jiter socket.
     * @return The IpAddress of the Jiter Socket.
     */
    public IpAddress getIpAddressFromAddress(Address address) {
        if (address == null) {
            return null;
        }

        @SuppressWarnings("unchecked")
        Map<Address, PhysicalAddress> addressMapping = (Map<Address, PhysicalAddress>) _channel.down(new Event(Event.GET_LOGICAL_PHYSICAL_MAPPINGS));
        PhysicalAddress physicalAddress = addressMapping.get(address);

        if (physicalAddress != null && 
            physicalAddress instanceof IpAddress) {
            InetAddress bindAddress = ((IpAddress) physicalAddress).getIpAddress();
            int port = ((IpAddress) physicalAddress).getPort();

            return new IpAddress(bindAddress, port);
        } else {
            return null;
        }
    }

    /**
     * Returns the name of the ISP to which the Jiter Socket is associated.
     * 
     * @return The name of the ISP to which the Jiter Socket is associated.
     */
    public String getIsp() {
        return _isp;
    }

    /**
     * Returns the View to which the Jiter Socket is associated.
     * 
     * @return The View to which the Jiter Socket is associated.
     */
    public View getView() {
        return _channel.getView();
    }

    /**
     * Takes in account a new View for the Jiter Socket.
     */
    public void viewAccepted(View newView) {
        _node.viewAccepted(newView);
    }

    /**
     * Allows the Jiter Socket to receive a message.
     * 
     * @param message The message received.
     */
    public void receive(Message message) {
        try {
            JiterMessage jiterMessage = JiterMessage.getJiterMessageFromString(message.getObject().toString());
            Log.getInstance().d("JiterSocket: Received message " + jiterMessage.getMessageId());
            Log.getInstance().d(jiterMessage.toString());

            IMessageHandler[] handlers = Config.getInstance().getActiveHandlers();

            for (int i = handlers.length - 1; i >= 0; i--) {
                if (handlers[i].handleReceiveMessage(this, jiterMessage)) {
                    break;
                }
            }
        } catch (JiterMessage.NotAJiterMessageException e) {
            Log.getInstance().e("Received non-jitter message: ");
            Log.getInstance().e(message.getObject().toString());
        }
    }
    
    /**
     * Allows a Jiter Socket to send a Jiter Message.
     * 
     * @param destinationAddress Address of the destination of the Message.
     * @param message The message.
     * @throws JiterSocketException
     */
    public void sendMessage(Address destinationAddress, JiterMessage message) throws JiterSocketException {
        message.setSourceAddress(getAddress());
        if (destinationAddress != null) {
            if (message.getDestinationName() == null) {
                message.setDestinationAddress(destinationAddress);
            }
        } else {
            if (message.getDestinationName() != null) {
                destinationAddress = _node.getAddressFromName(message.getDestinationName());
            } else {
                throw new JiterSocketException("Cannot send a message if the destination is not known");
            }
        }

        IMessageHandler[] handlers = Config.getInstance().getActiveHandlers();

        for (int i = 0; i < handlers.length; i++) {
            if (handlers[i].handleSendMessage(this, destinationAddress, message)) {
                break;
            }
        }
        forwardMessage(destinationAddress, message);
   
    }
    
    public void sendMessageThruChannel(IJiterChannel channel, JiterMessage message) throws JiterSocketException  {
    	
    	if (channel.getRelay() == null) {
    		sendMessage(channel.getDestination(), message);
    	} else {
    		message.setDestinationAddress(channel.getDestination());
    		message.setRelayIsp(channel.getIsps().get(1));
    		sendMessage(channel.getRelay(), message);
    	}
    }

    /**
     * Forwards a Jiter Message to a given Address.
     * 
     * @param destinationAddress The Addres of the destination of the Message.
     * @param message The Message.
     * @throws JiterSocketException
     */
    public void forwardMessage(Address destinationAddress, JiterMessage message) throws JiterSocketException {
        try {
            Message msg = new Message(destinationAddress, null, message.toString());
            _channel.send(msg);
            Log.getInstance().d("JiterSocket: Sent message " + message.getMessageId() + " to " + Utils.addressToLogicalName(destinationAddress));
        } catch (Exception e) {
            throw new JiterSocketException(e);
        }
    }

    /**
     * Returns a String representation of the Jiter Socket.
     * 
     * @return A String representation of the Jiter Socket.
     */
    public String toString() {
        return Utils.addressToLogicalName(getAddress()) + " (ISP: " + getIsp() + ", NodeID: " + Utils.addressToNodeId(getAddress()) + ", Bind Addr: " + _bindAddress.toString() + ")";
    }
    
	public void doPingPongUpdate(JiterDirectChannel channel) {
		JiterMessage message = new JiterMessage();
		message.setSourceAddress(channel.getSource());
		message.setDestinationAddress(channel.getDestination());
		message.setType(JiterMessage.Type.TXT);
		
		try {
			sendMessage(channel.getDestination(), message);
		} catch (JiterSocketException e) {
			Log.getInstance().e("Couldn't send the TXT message from node: "
					+ channel.getSourceNodeId() + " to node: " + channel.getDestinationNodeId());
			Log.getInstance().e(Utils.getStackTraceString(e));
		}
	}
}
