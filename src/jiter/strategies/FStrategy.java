package jiter.strategies;

import java.util.List;

import jiter.IJiterChannel;
import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocketException;
import jiter.misc.Log;
import jiter.misc.Utils;

public class FStrategy extends SendStrategy {
	public FStrategy(JiterNode node) {
        super(node);
	}
	
	@Override
	public void send(JiterMessage message, float deadline, String destination) {
		List<IJiterChannel> channelsToDestination = getNode().getOverlayChannelMatrix().
            getChannelsToDestination(destination);
		
		for (IJiterChannel channel : channelsToDestination) {
			try {
				internalSend(channel, message);
			} catch (JiterSocketException e) {
                super.sendFailed(message.getMessageId());
                Log.getInstance().i("FS Strategy: Sending message " + message.getMessageId() + " thru channel failed.");
                Log.getInstance().e(e.getMessage());
            	Log.getInstance().e(Utils.getStackTraceString(e));
			}
		}
	}

    @Override
    public String toString() {
        return "F";
    }
}
