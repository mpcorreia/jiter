package jiter.strategies;

import jiter.JiterNode;

/**
 * This class implements the J1 Strategy.
 * The algorithm is the presented in the paper but only 1
 * backup channel is used.
 *  
 * @author Rui Silva
 *
 */
public class J1Strategy extends JPaperStrategy {
	
	public J1Strategy(JiterNode node) throws Exception {
		super(node);
		_numberOfDesiredNodes = 1;
	}

    @Override
    public String toString() {
        return "J1";
    }
}
