package jiter.strategies;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jiter.IJiterChannel;
import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocketException;
import jiter.handlers.AckHandler;

import org.jgroups.Address;

import jiter.misc.Config;
import jiter.misc.Log;
import jiter.misc.Utils;

/**
 * This strategy implements the Best-Path algorithm.
 * According to this algorithm, a message should always be sent through the best overlay channel.
 * In case of failures the message should be retransmited at most 3 times using a fixed timeout
 * of 3 seconds. The retransmissions should always use the best overlay channel.
 * 
 * @author Rui Silva
 *
 */
public class BPStrategy extends SendStrategy {
    private static final int ACK_TIMEOUT = 3000;
    private static final int MAX_RETRIES = 3;
	
    private Map<Long, Integer> _numberRetries = null;
	
    public BPStrategy(JiterNode node) throws Exception {
        super(node);
        _numberRetries = new HashMap<Long, Integer>();
    }

    @Override
    public void initialize() throws Exception {
        super.initialize();
        Config config = Config.getInstance();
        config.setAckTimeoutInterval(ACK_TIMEOUT);
    }

	@Override
	public void send(JiterMessage message, float deadline, String destination) {
        _numberRetries.put(message.getMessageId(), 0);
        
        privateSend(message, destination);
	}

    public void sendFailed(AckHandler.UnackedMessageData umd) {
        JiterMessage message = umd.getOriginalMessage();
        long messageId = message.getMessageId();
        Integer numberRetries = _numberRetries.get(messageId);
        Address destination = getNode().getAddressFromName(umd.getOriginalMessage().getDestinationName());
        String destinationNodeId = Utils.addressToNodeId(destination);

        if (numberRetries != null && numberRetries < MAX_RETRIES) {
            _numberRetries.put(messageId, ++numberRetries);

            privateSend(message, destinationNodeId);
        } else {
            _numberRetries.put(messageId, null);
            Log.getInstance().i("BP Strategy: Failed to send message " + messageId + " after 3 retransmissions.");
            super.sendFailed(messageId);
        }
    }

    private void privateSend(JiterMessage message, String destination) {
		List<IJiterChannel> channelsToDestination = getNode().getOverlayChannelMatrix().getChannelsToDestination(
    			destination);
		IJiterChannel bestChannel = channelsToDestination.get(0);
		Log.getInstance().d("BP Strategy: Sending thru direct channel: ");
		Log.getInstance().d(bestChannel.toDetailedString());
    	
        try {
            internalSend(bestChannel, message);
		} catch (JiterSocketException e) {
            super.sendFailed(message.getMessageId());
            Log.getInstance().i("BP Strategy: Sending message " + message.getMessageId() + " thru best channel failed.");
            Log.getInstance().e(e.getMessage());
        	Log.getInstance().e(Utils.getStackTraceString(e));
		}
    }

    public String toString() {
        return "BP";
    }
}
