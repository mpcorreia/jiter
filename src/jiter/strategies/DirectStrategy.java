package jiter.strategies;

import jiter.IJiterChannel;
import jiter.JiterMessage;
import jiter.JiterNode;

import java.util.List;

import org.jgroups.Address;

import jiter.misc.Log;
import jiter.misc.Utils;

public class DirectStrategy extends SendStrategy {

    public DirectStrategy(JiterNode node) {
        super(node);
    }

    public void send(JiterMessage message, float deadline, String destination) {
        List<IJiterChannel> possibleChannels = getNode().getOverlayChannelMatrix().getChannelsToDestination(destination);

        try {
            IJiterChannel directChannel = possibleChannels.get(0);
            super.internalSend(directChannel, message);
        } catch (Exception e) {
        	Log.getInstance().e(e.getMessage());
        	Log.getInstance().e(Utils.getStackTraceString(e));
            super.sendFailed(message.getMessageId());
        }
    }
    
    public void sendTestMessage(JiterMessage message, Address destinationAddress) {
        try {
        	getUnackedMessageIds().add(message.getMessageId());
        	getNode().getTestJiterSocket().sendMessage(destinationAddress, message);
        } catch (Exception e) {
        	Log.getInstance().e(e.getMessage());
        	Log.getInstance().e(Utils.getStackTraceString(e));
            super.sendFailed(message.getMessageId());
        }
    }

    @Override
    public String toString() {
        return "Direct";
    }
}
