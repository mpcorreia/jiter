package jiter.strategies;

import java.util.HashSet;
import java.util.Set;

import jiter.handlers.AckHandler;
import jiter.handlers.IMessageHandler;

import jiter.IJiterChannel;
import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocket;
import jiter.JiterSocketException;

import jiter.misc.Config;
import jiter.misc.Log;

public abstract class SendStrategy implements AckHandler.IAckObserver {
    private JiterNode _node;
    private Set<Long> _unackedMessageIds;
    protected AckHandler _ackHandler;

    public SendStrategy(JiterNode node) {
        _node = node;
        _unackedMessageIds = new HashSet<Long>();
        _ackHandler = null;
    }

    public void initialize() throws Exception {
        Config config = Config.getInstance();

        for (IMessageHandler handler : config.getActiveHandlers()) {
            if (handler instanceof AckHandler) {
                _ackHandler = (AckHandler) handler;
                break;
            }
        }

        if (_ackHandler == null) {
            throw new Exception("Cannot use send strategy with no ack handler set");
        }

        _ackHandler.registerObserver(this);
    }

    public void dispose() throws Exception {
        _ackHandler.unregisterObserver(this);
    }

    public abstract void send(JiterMessage message, float deadline, String destination);

    public void sendSuccessful(AckHandler.UnackedMessageData umd) {
        sendSuccessful(umd.getOriginalMessage().getMessageId());
    }

    public void sendSuccessful(long messageId) {
        if (_unackedMessageIds.contains(messageId)) {
            _unackedMessageIds.remove(messageId);
            _node.notifySendSuccessful(messageId);
        }
    }

    public void sendFailed(AckHandler.UnackedMessageData umd) {
        sendFailed(umd.getOriginalMessage().getMessageId());
    }

    public void sendFailed(long messageId) {
        if (_unackedMessageIds.contains(messageId)) {
            _unackedMessageIds.remove(messageId);
            _node.notifySendFailed(messageId);
        }
    }

    protected void internalSend(IJiterChannel channel, JiterMessage message) throws JiterSocketException {
        assert message != null : "Message cannot be null";
        assert channel != null : "Channel cannot be null";

        try {
            _unackedMessageIds.add(message.getMessageId());
            JiterSocket socket = _node.getLocalSocket(channel.getSource());
            Log.getInstance().countNewMessage();
            socket.sendMessageThruChannel(channel, message);
        } catch (JiterSocketException e) {
            throw e;
        }
    }

    /**
     * Gets the node for this instance.
     *
     * @return The node.
     */
    public JiterNode getNode() {
        return _node;
    }

    /**
     * Sets the node for this instance.
     *
     * @param node The node.
     */
    public void setNode(JiterNode node) {
        _node = node;
    }
    
    public Set<Long> getUnackedMessageIds() {
    	return _unackedMessageIds;
    }
}
