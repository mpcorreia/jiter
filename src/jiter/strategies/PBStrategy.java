package jiter.strategies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jiter.IJiterChannel;
import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.handlers.AckHandler;

import org.jgroups.Address;

import jiter.misc.Config;
import jiter.misc.Log;
import jiter.misc.Utils;

/**
 * This strategy implements the Primary-Backup algorithm
 *
 * @author Alexandre Fonseca
 *
 */
public class PBStrategy extends SendStrategy {
    private static final int ACK_TIMEOUT = 3000;
    private static final int MAX_RETRIES = 3;
	
    private IJiterChannel _activeChannel = null;
    private Map<Long, Integer> _numberRetries = null;
	
    public PBStrategy(JiterNode node) throws Exception {
        super(node);
        _numberRetries = new HashMap<Long, Integer>();
    }

    public void initialize() throws Exception {
        super.initialize();
        Config.getInstance().setAckTimeoutInterval(ACK_TIMEOUT);
    }

	@Override
	public void send(JiterMessage message, float deadline, String destination) {
        if (_activeChannel == null) {
            _activeChannel = Utils.getRandomListItem(getNode().getOverlayChannelMatrix().getChannelsToDestination(destination));
        }
		
        _numberRetries.put(message.getMessageId(), 0);

        sendPrivate(message);
	}

    public void sendFailed(AckHandler.UnackedMessageData umd) {
        JiterMessage message = umd.getOriginalMessage();
        long messageId = message.getMessageId();
        Integer numberRetries = _numberRetries.get(messageId);
        Address destination = getNode().getAddressFromName(umd.getOriginalMessage().getDestinationName());
        String destinationNodeId = Utils.addressToNodeId(destination);
        if (numberRetries != null && numberRetries < MAX_RETRIES) {
            List<IJiterChannel> availableChannels = new ArrayList<IJiterChannel>(getNode().getOverlayChannelMatrix().getChannelsToDestination(destinationNodeId));
            availableChannels.remove(_activeChannel);
            
            _activeChannel = Utils.getRandomListItem(availableChannels);
            _numberRetries.put(messageId, numberRetries++);

            sendPrivate(message);
        } else {
            _numberRetries.put(messageId, null);
            super.sendFailed(umd);
        }
    };

    private void sendPrivate(JiterMessage message) {
        try {
            internalSend(_activeChannel, message);
		} catch (Exception e) {
            super.sendFailed(message.getMessageId());
			Log.getInstance().i("PB Strategy: Sending message " + message.getMessageId() + " thru channel failed.");
            Log.getInstance().e(e.getMessage());
        	Log.getInstance().e(Utils.getStackTraceString(e));
		}
    }

    @Override
    public String toString() {
        return "PB";
    }
}
