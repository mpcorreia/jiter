package jiter.strategies;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

import jiter.IJiterChannel;
import jiter.JiterDirectChannel;
import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocketException;
import jiter.handlers.AckHandler;

import jiter.misc.Config;
import jiter.misc.Log;
import jiter.misc.Utils;

/**
 * This class implements the SOSR send strategy.
 * 
 * @author Alexandre Fonseca
 *
 */
public class SOSRStrategy extends SendStrategy {

    private static final int ACK_TIMEOUT = 3000;
	protected Timer _timer = null;
	protected int  _numberOfDesiredNodes = 0;
    protected List<Long> _firstMessages;
	
	public SOSRStrategy(JiterNode node) throws Exception {
        super(node);
		_timer = new Timer(true);
        _firstMessages = new LinkedList<Long>();
	}

    public void initialize() throws Exception {
        super.initialize();
        Config config = Config.getInstance();
        config.setAckTimeoutInterval(ACK_TIMEOUT);
    }

	public void send(JiterMessage message, float deadline, String destination) {
        List<JiterDirectChannel> directChannels = getNode().getDirectChannelMatrix().getChannelsWithDestinationNode(destination);
        JiterDirectChannel firstChannel = Utils.getRandomListItem(directChannels);

        try {
            _firstMessages.add(message.getMessageId());

            internalSend(firstChannel, message);

        } catch (JiterSocketException e) {
            super.sendFailed(message.getMessageId());
			Log.getInstance().i("SOSR Strategy: Sending message " + message.getMessageId() + " thru direct channel failed.");
            Log.getInstance().e(e.getMessage());
        	Log.getInstance().e(Utils.getStackTraceString(e));
        }
	}

    public void sendFailed(AckHandler.UnackedMessageData umd) {
        if (_firstMessages.contains(umd.getOriginalMessage().getMessageId())) {
            _firstMessages.remove(umd.getOriginalMessage().getMessageId());
            String destinationNodeId = Utils.addressToNodeId(umd.getDestinationAddress());
            List<IJiterChannel> randomBackupChannels = Utils.getRandomSubList(getNode().getOverlayChannelMatrix().getChannelsToDestination(destinationNodeId), 4);

            boolean oneSendSuccessful = false;

/*    		System.out.println("*** The current Overlay Matrix for the destination: ***");
    		Utils.printCollection(_node.getOverlayChannelMatrix().getChannelsToDestination(umd.getDestinationAddress()), 
    				new Utils.Stringer<IJiterChannel>() {

    					@Override
    					public String string(IJiterChannel data) {
    						return data.toDetailedString();
    					}
    				});
    		System.out.println("*** End of the current Overlay Matrix for the destination: ***");
    		System.out.println(Utils.currentTime() + "SOSRStrategy: Sending thru direct channel: ");*/
    		
            for (IJiterChannel channel : randomBackupChannels) {
                try {

                    internalSend(channel, umd.getOriginalMessage());
                    oneSendSuccessful = true;

                } catch (JiterSocketException e) {
        			Log.getInstance().i("SOSR Strategy: Sending message " + umd.getOriginalMessage().getMessageId() + " thru random channel failed.");
                    Log.getInstance().e(e.getMessage());
                	Log.getInstance().e(Utils.getStackTraceString(e));
                }
            }

            if (!oneSendSuccessful) {
                super.sendFailed(umd);
            }
        } else {
            super.sendFailed(umd);
        }
    };

    @Override
    public String toString() {
        return "SOSR";
    }
}

