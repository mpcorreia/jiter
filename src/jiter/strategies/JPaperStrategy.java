package jiter.strategies;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import jiter.IJiterChannel;
import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocketException;
import jiter.misc.Log;
import jiter.misc.Utils;

/**
 * This class represents an abstract strategy that uses
 * the algorithm present in the Jiter paper. It's purpose is
 * to encapsulate the common parts of the Jiter, Jiter0 and Jiter1
 * algorithms.
 * 
 * @author Pedro Marques da Luz
 * @author Rui Silva
 *
 */
public abstract class JPaperStrategy extends SendStrategy {
	protected List<Timer> _timers = null;
	protected int  _numberOfDesiredNodes = 0;
	
	public JPaperStrategy(JiterNode node) throws Exception {
        super(node);
		_timers = Collections.synchronizedList(new ArrayList<Timer>());
	}

	public void send(JiterMessage message, float deadline, String destination) {
		List<IJiterChannel> channelsToDestination = getNode().getOverlayChannelMatrix().getChannelsToDestination(
    			destination);

    	// None of the channels to the destination have a txt that meets the deadline.
    	if (channelsToDestination.get(0).getTxt() > deadline) {
            //getNode().notifySendFailed(message.getMessageId());  //mpc: should be uncommented if original algorithm 
            try {   //mpc: this try-catch should be commented if original algorithm 
                Thread.sleep((long)deadline+(long)100); 
            } catch (InterruptedException e) {
                Log.getInstance().e("Sleep until end of deadline failed");
                Log.getInstance().e(e.getMessage());
            }
    		return;
    	}
    	
    	int baseChannel = 0;
    	double timeElapsed = 2 * channelsToDestination.get(0).getTxt();  //mpc: 2 should be 1 if original algorithm 
        float deadlineWithMargin = deadline; // * 0.9f;  mpc
    	
    	// Can be a foreach
    	while (baseChannel + 1 < channelsToDestination.size()) {
    		if (timeElapsed + (2 * channelsToDestination.get(baseChannel + 1).getTxt()) < deadlineWithMargin) {
    			timeElapsed += (2 * channelsToDestination.get(baseChannel + 1).getTxt());
    			baseChannel += 1;
    		} else {
    			break;
    		}
    	}


    	
        boolean oneSendSuccessful = false;
    	try {
			internalSend(channelsToDestination.get(baseChannel), message);
			Log.getInstance().addNewJiterChannelUsed(channelsToDestination.get(baseChannel));
			oneSendSuccessful = true;
		} catch (JiterSocketException e) {
			Log.getInstance().i(this.toString() + ": Sending message + " + message.getMessageId() + " thru base channel failed");
			Log.getInstance().e(e.getMessage());
        	Log.getInstance().e(Utils.getStackTraceString(e));
		}
    	
    	// First Condition
    	List<IJiterChannel> backupChannels = getNode().getOverlayChannelMatrix().getChannelsToDestination(destination, deadlineWithMargin);

        if (backupChannels.size() < _numberOfDesiredNodes) {
            backupChannels = getNode().getOverlayChannelMatrix().getChannelsToDestination(destination, deadline);
        }

    	// Now we should choose n channels. The n less correlated
    	backupChannels = getNLessCorrelated(channelsToDestination.get(baseChannel),
    			_numberOfDesiredNodes, backupChannels);
    	
    	for(IJiterChannel channel : backupChannels) {
    		try {

    			internalSend(channel, message);
    			Log.getInstance().addNewJiterChannelUsed(channel);
                oneSendSuccessful = true;

    		} catch (JiterSocketException e) {
    			Log.getInstance().i(this.toString() + ": Sending message + " + message.getMessageId() + " thru backup channel failed");
                Log.getInstance().e(e.getMessage());
            	Log.getInstance().e(Utils.getStackTraceString(e));
    		}
    	}

        if (!oneSendSuccessful) {
            super.sendFailed(message.getMessageId());
            return;
        }

    	int newDeadline = (int)(deadline - (2 * channelsToDestination.get(baseChannel).getTxt()));
    	
    	// Launch timer
		
    	Timer timer = new Timer(true);
    	
    	Date now = new Date();
    	
    	now.setTime(now.getTime() + newDeadline);
    	
    	timer.schedule(new TimeoutHandlerTask(message, newDeadline, destination), now);
    	
    	_timers.add(timer);
	}

    private class TimeoutHandlerTask extends TimerTask {
        JiterMessage _message = null;
        float _deadline = 0;
        String _destinationNodeId = null;
        
        public TimeoutHandlerTask(JiterMessage message, float deadline, String destinationNodeId) {
            _message = message;
            _deadline = deadline;
            _destinationNodeId = destinationNodeId;
        }
        
        @Override
        public void run() {
            // Check if message wasn't already received
            if (_ackHandler.isMessageUnacked(_message.getMessageId())) {
                // If so, rerun the send strategy
                send(_message, _deadline, _destinationNodeId);
            }
        }
    }
	
	public List<IJiterChannel> getNLessCorrelated (IJiterChannel baseChannel, int desiredChannels, List<IJiterChannel> backup) {
        assert baseChannel != null : "Base channel should not be null";
        assert backup != null : "Backup should not be null";

    	List<IJiterChannel> result = new ArrayList<IJiterChannel>();
    	List<InetAddress> routers = new ArrayList<InetAddress>(baseChannel.getRoute());
    	
    	for (int i = 0; i < desiredChannels; i++) {
    		float[] localCorrelation = new float[backup.size()];
    		
    		for (int h = 0; h < backup.size(); h++) {
                assert backup.get(h) != null : "Backup list element at index " + h + " should not be null";
    			localCorrelation[h] = correlation(routers, backup.get(h).getRoute());
    		}
    		
    		//Get Minimum
    		int position = 0;
    		IJiterChannel minimum = backup.get(position);
    		for (int c = 0; c < localCorrelation.length; c++) {
    			if( localCorrelation[c] < localCorrelation[position]) {
    				position = c;
    				minimum = backup.get(c);
    			}
    		}

            assert minimum != null : "Minimum should not be null here";
    		
    		routers.addAll(minimum.getRoute());
    		result.add(minimum);
    		//backup.remove(minimum);
    	}

		return result;
    }
    
    public float correlation(List<InetAddress> routers, List<InetAddress> channelRouters) {
    	int match = 0;
    	
    	for (InetAddress address : routers) {
			for (InetAddress addressChannel : channelRouters) {
				//System.out.println("HI!" + address.toString() + " " + addressChannel.toString());
				if (addressChannel.equals(address)) {
					//System.out.println("MATCH!!");
					match += 1;
				}
			}
		}
    	
    	return (float) match / ( (routers.size() + channelRouters.size()) / 2);
    }

}

