package jiter.strategies;

import jiter.JiterNode;

/**
 * This class implements the J3 Strategy.
 * The algorithm is the presented in the paper but only 3
 * 
 * @author Rui Silva
 *
 */
public class J3Strategy extends JPaperStrategy {

	public J3Strategy(JiterNode node) throws Exception {
		super(node);
		_numberOfDesiredNodes = 3;
	}

    @Override
    public String toString() {
        return "J3";
    }
}
