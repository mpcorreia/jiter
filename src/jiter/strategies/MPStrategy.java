package jiter.strategies;

import java.util.ArrayList;
import java.util.List;

import jiter.IJiterChannel;
import jiter.JiterDirectChannel;
import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocketException;
import jiter.misc.Log;
import jiter.misc.Utils;

/**
 * This class implements the Multi-Path Strategy (MP).
 * In this algorithm a message is sent thru a direct channel and thru another overlay channel
 * (direct or indirect).
 * There are no retransmissions nor need to wait for acks.
 *  
 * @author Rui Silva
 *
 */
public class MPStrategy extends SendStrategy {

	public MPStrategy(JiterNode node) {
        super(node);
	}
	
	@Override
	public void send(JiterMessage message, float deadline, String destination) {
        List<JiterDirectChannel> directChannels = getNode().getDirectChannelMatrix().getChannelsWithDestinationNode(destination);
        JiterDirectChannel directChannel = Utils.getRandomListItem(directChannels);
        
        // prevent from choosing the same channel
        List<IJiterChannel> availableChannels = new ArrayList<IJiterChannel>(getNode().getOverlayChannelMatrix().getChannelsToDestination(destination));
        availableChannels.remove(directChannel);
		IJiterChannel randomChannel = 
				Utils.getRandomListItem(availableChannels);
		
		//System.out.println("*** The current Overlay Matrix for the destination: ***");
		/*Utils.printCollection(_node.getOverlayChannelMatrix().getChannelsToDestination(destination), 
				new Utils.Stringer<IJiterChannel>() {

					@Override
					public String string(IJiterChannel data) {
						return data.toDetailedString();
					}
				});*/
		//System.out.println("*** End of the current Overlay Matrix for the destination: ***");

        boolean oneSendSuccesful = false;
		
		try {
			//System.out.println("MPStrategy: Sending thru direct channel: ");
			//System.out.println(directChannel.toDetailedString());
			internalSend(directChannel, message);
            oneSendSuccesful = true;
		} catch (JiterSocketException e) {
			Log.getInstance().i("MPStrategy: Sending message " + message.getMessageId() + " thru direct channel failed.");
            Log.getInstance().e(e.getMessage());
        	Log.getInstance().e(Utils.getStackTraceString(e));
		}
		
		try {
			if (randomChannel != null) {
				//System.out.println("MPStrategy: Sending thru random channel: ");
				//System.out.println(randomChannel.toDetailedString());
				internalSend(randomChannel, message);
                oneSendSuccesful = true;
			}
		} catch (JiterSocketException e) {
			Log.getInstance().i("MPStrategy: Sending message " + message.getMessageId() + " thru random overlay channel failed.");
            Log.getInstance().e(e.getMessage());
        	Log.getInstance().e(Utils.getStackTraceString(e));
		}

        if (!oneSendSuccesful) {
            super.sendFailed(message.getMessageId());
        }
	}

    @Override
    public String toString() {
        return "MP";
    }
}
