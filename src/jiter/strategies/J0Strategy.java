package jiter.strategies;

import jiter.JiterNode;

/**
 * This class implements the J0 Strategy.
 * The algorithm is the presented in the paper no
 * backup channels are used.
 *  
 * @author Rui Silva
 *
 */
public class J0Strategy extends JPaperStrategy {

	public J0Strategy(JiterNode node) throws Exception {
		super(node);
		_numberOfDesiredNodes = 0;
	}

    @Override
    public String toString() {
        return "J0";
    }
}
