package jiter.misc;
import java.util.LinkedList;
import java.util.List;


/**
 * This class allows to calculate a simple moving average,
 * which is used in the update of the txt of the Jiter Direct Channels.
 * 
 * @author Rui Silva
 * @version 1.0
 *
 */
public class MovingAverageHelper{

	/** Number of samples to consider. */
	private int _maxNumberSamples;
	
	/** Total so far. */
    private float _total = 0;
    
    /** Samples collected. */
    private List<Double> _samples;

    /**
     * Creates an instance of a Moving Average Helper.
     * 
     * @param numberSamples Number of samples to consider.
     */
    public MovingAverageHelper(int numberSamples) {
        _maxNumberSamples = numberSamples;
        _samples = new LinkedList<Double>();
    }

    /**
     * Adds a sample.
     * 
     * @param sample The sample to add.
     */
    public void add(double sample) {
    	_total += sample;
    	_samples.add(sample);
    	
    	if (_samples.size() > _maxNumberSamples) {
    		_total -= _samples.remove(0);
    	}
    }

    /**
     * Returns the average calculated.
     * 
     * @return The average calculated.
     */
    public float getAverage() {
    	return _total / _samples.size();
    }
}
