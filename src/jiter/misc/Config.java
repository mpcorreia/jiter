package jiter.misc;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import jiter.JiterNode;
import jiter.handlers.AckHandler;
import jiter.handlers.DCHandler;
import jiter.handlers.DataHandler;
import jiter.handlers.IMessageHandler;
import jiter.handlers.RelayHandler;
import jiter.strategies.BPStrategy;
import jiter.strategies.FStrategy;
import jiter.strategies.J0Strategy;
import jiter.strategies.J1Strategy;
import jiter.strategies.MPStrategy;
import jiter.strategies.PBStrategy;
import jiter.strategies.SOSRStrategy;
import jiter.strategies.SendStrategy;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

/**
 * Class that serves as a central repository of configurable options and
 * values.
 * 
 * @author Alexandre Fonseca
 * @version 1.0
 *
 */
public class Config {
    private static Config _instance = null;

    private JiterNode _node;
    private SendStrategy _sendStrategy;
    private String _globalCommunicationGroup;
    private String _testCommunicationGroup;
    private IMessageHandler[] _activeHandlers;
    private int _traceRouteUpdateInterval;
    private int _movingAverageSamples;
    private int _txtUpdateInterval;
    private int _maximumNoTxtUpdateInterval;
    private int _ackTimeoutInterval;
    private boolean _autoCreateSockets;
    private List<InetAddress> _ignoredInterfaces;
    private boolean _localhostOnly;
    private long _experimentMessageId;
    private boolean _resolvePublicIps;

    public static Config initialize(JiterNode node) throws Exception {
        if (_instance == null) {
            _instance = new Config(node);
        }
        
        return _instance;
    }

    public static Config getInstance() {
        return _instance;
    }

    private Config(JiterNode node) throws Exception {
        _node = node;
        _globalCommunicationGroup = "jiter";
        _testCommunicationGroup = "test";
        _activeHandlers = new IMessageHandler[] {
        	new DataHandler(),
        	new DCHandler(_node),
        	new AckHandler(),
        	new RelayHandler() 
        };
        _traceRouteUpdateInterval = 300000; // 5 minutes
        _movingAverageSamples = 5;
        _txtUpdateInterval = 90000; // 1,5 minutes
        _maximumNoTxtUpdateInterval = 60000; // 1 minute
        _ackTimeoutInterval = 3000;
        _sendStrategy = new J1Strategy(_node);
        _autoCreateSockets = false;
        _ignoredInterfaces = new LinkedList<InetAddress>();
        _experimentMessageId = -1;
    }

    public Config parseArguments(String[] args) {
        Config result = CliFactory.parseArgumentsUsingInstance(this, args);

        return result;
    }

    public SendStrategy getSendStrategy() {
        return _sendStrategy;
    }

    public void setSendStrategy(SendStrategy sendStrategy) throws Exception {
        if (_sendStrategy != null) {
            _sendStrategy.dispose();
        }
        _sendStrategy = sendStrategy;
        _sendStrategy.initialize();
    }

    @Option(shortName="s", defaultToNull=true)
    public void setSendStrategy(String sendStrategyString) throws Exception {
        SendStrategy strategy = null;

        if (sendStrategyString.equals("BP")) {
            strategy = new BPStrategy(_node);
        }
        else if (sendStrategyString.equals("J0")) {
            strategy = new J0Strategy(_node);
        }
        else if (sendStrategyString.equals("J1")) {
            strategy = new J1Strategy(_node);
        }
        else if (sendStrategyString.equals("F")) {
            strategy = new FStrategy(_node);
        }
        else if (sendStrategyString.equals("MP")) {
            strategy = new MPStrategy(_node);
        }
        else if (sendStrategyString.equals("SOSR")) {
            strategy = new SOSRStrategy(_node);
        }
        else if (sendStrategyString.equals("RR")) {
            //strategy = new FStrategy(_node);
        }
        else if (sendStrategyString.equals("PB")) {
            strategy = new PBStrategy(_node);
        } else {
            throw new Exception("Unknown send strategy: " + sendStrategyString);
        }

        _sendStrategy = strategy;
    }

    public String getGlobalCommunicationGroup() {
        return _globalCommunicationGroup;
    }

    @Option(shortName="g", defaultToNull=true, description="The communication group used by the node's sockets")
    public void setGlobalCommunicationGroup(String globalCommunicationGroup) {
        _globalCommunicationGroup = globalCommunicationGroup;
    }
    
    public String getTestCommunicationGroup() {
        return _testCommunicationGroup;
    }

    public IMessageHandler[] getActiveHandlers() {
        return _activeHandlers;
    }

    public void setActiveHandlers(IMessageHandler[] activeHandlers) {
        _activeHandlers = activeHandlers;
    }

    public int getTraceRouteUpdateInterval() {
        return _traceRouteUpdateInterval;
    }

    @Option(shortName="u", defaultToNull=true, description="The time interval between trace route updates (miliseconds)")
    public void setTraceRouteUpdateInterval(int traceRouteUpdateInterval) {
        _traceRouteUpdateInterval = traceRouteUpdateInterval;
    }

    public int getMovingAverageSamples() {
        return _movingAverageSamples;
    }

    @Option(shortName="s", defaultToNull=true, description="The number of samples to use on the moving average calculation")
    public void setMovingAverageSamples(int movingAverageSamples) {
        _movingAverageSamples = movingAverageSamples;
    }

    public int getTxtUpdateInterval() {
        return _txtUpdateInterval;
    }

    @Option(shortName="t", defaultToNull=true, description="The time interval between txt update checks (miliseconds)")
    public void setTxtUpdateInterval(int txtUpdateInterval) {
        _txtUpdateInterval = txtUpdateInterval;
    }

    public int getMaximumNoTxtUpdateInterval() {
        return _maximumNoTxtUpdateInterval;
    }

    @Option(shortName="n", defaultToNull=true, description="The maximum allowed time without txt updates (miliseconds)")
    public void setMaximumNoTxtUpdateInterval(int maximumNoTxtUpdateInterval) {
        _maximumNoTxtUpdateInterval = maximumNoTxtUpdateInterval;
    }

    public int getAckTimeoutInterval() {
        return _ackTimeoutInterval;
    }

    @Option(shortName="k", defaultToNull=true, description="The interval of time after which a message is considered lost if no ack was received")
    public void setAckTimeoutInterval(int ackTimeoutInterval) {
        _ackTimeoutInterval = ackTimeoutInterval;
    }

    @Option(helpRequest = true)
    public void setHelp(boolean help) {
    }

    public boolean isAutoCreateSockets() {
        return _autoCreateSockets;
    }

    @Option(shortName="a", description="Should the node automatically create sockets for each detected interface")
    public void setAutoCreateSockets(boolean autoCreateSockets) {
        _autoCreateSockets = autoCreateSockets;
    }

    @Option(shortName="i", defaultToNull=true, description="Addresses of the interfaces that should be ignored")
    public void setIgnoredInterfaces(List<String> ignoredInterfaces) {
        _ignoredInterfaces.clear();
        for (String ignoredInterface : ignoredInterfaces) {
            try {
            InetAddress address = InetAddress.getByName(ignoredInterface);
            _ignoredInterfaces.add(address);
            } catch (UnknownHostException e) {
                Log.getInstance().e(e.getMessage());
            }
        }
    }

    public List<InetAddress> getIgnoredInterfaces() {
        return _ignoredInterfaces;
    }

    public boolean isLocalhostOnly() {
        return _localhostOnly;
    }

    @Option(shortName="l", description="Only use localhost")
    public void setLocalhostOnly(boolean localhostOnly) {
        _localhostOnly = localhostOnly;
    }

    @Option(shortName="r", description="Resolve public ips")
    public void setResolvePublicIps(boolean resolvePublicIps) {
        _resolvePublicIps = resolvePublicIps;
    }

    public boolean isResolvePublicIps() {
        return _resolvePublicIps;
    }
    
    public void setExperimentMessageId(long id) {
    	_experimentMessageId = id;
    }
    
    public long getExperimentMessageId() {
    	return _experimentMessageId;
    }
}
