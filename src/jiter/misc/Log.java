package jiter.misc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import jiter.IJiterChannel;

/**
 * Logger of the events that happen during the experiments.
 * Differentiates between debug, important, error and result messages.
 * 
 * @author Rui Silva
 * @version 0.1
 *
 */
public class Log {
	
	// *** Files' attributes ***
	/** Flag that indicates if everything should be printed on System Out */
	private boolean _debugMode;
	
	/** Flag that indicates if errors should be printed on file or system err */
	private boolean _errorsOnFile;
	
	/** Flag that indicates if important messages should be printed on file or system out */
	private boolean _importantsOnFile;
	
	/** Path of the error file. */
	private final static String ERRORS_FILE_PATH = "error.";
	
	/** Path of the importants file. */
	private final static String IMPORTANTS_FILE_PATH = "important.";
	
	/** Path of the debug file. */
	private final static String DEBUG_FILE_PATH = "debug.";
	
	/** Path of the experiments' results file. */
	private final static String RESULTS_FILE_PATH = "results.";
	
	/** Writer of the errors file. */
	private Writer _errorsFileWriter;
	
	/** Writer of the importants file. */
	private Writer _importantsFileWriter;
	
	/** Writer of the debug file. */
	private Writer _debugFileWriter;
	
	/** Writer of the results file. */
	private Writer _resultsFileWriter;
	
	// *** Experiments' attributes ***
	/** Name of the node that is sending the message. */
	private String _source;
	
	/** Name of the node that is receiving the message. */
	private String _destination;
	
	/** Deadline of the current experiment. */
	private float _deadline;
	
	/** Name of the algorithm of the current experiment. */
	private String _algorithm;
	
	/** Time to receive the ACK. */
	private float _ackTime;
	
	/** Counter of the number of messages exchanged during the experiment. */
	private int _messagesCounter;
	
	// *** Jiter specific attributes ***
	/** List of the jiter channels used. */
	private List<IJiterChannel> _jiterChannelsUsed;
	
	private static Log _log = null;
	
	/**
	 * Creates an instance of Log.
	 */
	private Log(boolean errorsOnFile, boolean importantsOnFile, boolean debugMode) {
		_errorsOnFile = errorsOnFile;
		_importantsOnFile = importantsOnFile;
		_debugMode = debugMode;
		
		try {
			if (_importantsOnFile) {
				String importantsPath = getLogFileName(IMPORTANTS_FILE_PATH);
				new File(importantsPath).createNewFile();
				_importantsFileWriter = new OutputStreamWriter(new FileOutputStream(importantsPath));
			}
			if (_errorsOnFile) {
				String errorPath = getLogFileName(ERRORS_FILE_PATH);
				new File(errorPath).createNewFile();
				_errorsFileWriter = new OutputStreamWriter(new FileOutputStream(errorPath));
			}
			String debugPath = getLogFileName(DEBUG_FILE_PATH);
			new File(debugPath).createNewFile();
			_debugFileWriter = new OutputStreamWriter(new FileOutputStream(debugPath));
			
			String resultsPath = getLogFileName(RESULTS_FILE_PATH);
			new File(resultsPath).createNewFile();
			_resultsFileWriter = new OutputStreamWriter(new FileOutputStream(resultsPath));
			
		} catch (IOException e) {
			System.out.println(getMessagePrefix() + "Severe Error: Could not open the required files.");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(getMessagePrefix() + "Severe Error: Exception while opening files.");
			e.printStackTrace();
		}
	}
	
	public static Log getInstance() {
		if (_log == null) {
			_log = new Log(true, true, false); //mpc
		}
		
		return _log;
	}
	
	/**
	 * Prints to file or system.out the debug msg.
	 * 
	 * @param msg Debug message to print.
	 */
	public void d(String msg) {
		String printMsg = getMessagePrefix() + "Debug: " + msg;
		
		try {
			print(_debugFileWriter, printMsg);
		} catch (IOException e) {
			System.out.println(getMessagePrefix() + "Severe Error: Could not write debug file.");
			e.printStackTrace();
		}
		
		if (_debugMode) {
			System.out.println(printMsg);
		}
	}
	
	/**
	 * Prints to System.out or System.err or file the error message.
	 * 
	 * @param msg Error message to print.
	 */
	public void e(String msg) {
		String printMsg = getMessagePrefix() + "Error: " + msg;
		
		if (_errorsOnFile) {
			
			try {
				print(_errorsFileWriter, printMsg);
				// just for debug purposes everything will be written to debug file.
				// print(_debugFileWriter, printMsg);  //mpc
			} catch (IOException e) {
				System.out.println(getMessagePrefix() + "Severe Error: Could not write error file.");
				e.printStackTrace();
			}
			
		} else {
			System.err.println(printMsg);
		}
		
		if (_debugMode) {
			System.out.println(printMsg);
		}
	}
	
	/**
	 * Prints to system.out or file the important message.
	 * 
	 * @param msg Important message to print.
	 */
	public void i(String msg) {
		String printMsg = getMessagePrefix() + "Important: " + msg;
		
		if (_importantsOnFile) {
			
			try {
				print(_importantsFileWriter, printMsg);
				// just for debug purposes everything will be written to debug file.
				//print(_debugFileWriter, printMsg); //mpc
			} catch (IOException e) {
				System.out.println(getMessagePrefix() + "Severe Error: Could not write importants file.");
				e.printStackTrace();
			}
			
			if (_debugMode) {
				System.out.println(printMsg);
			}
			
		} else {
			System.out.println(printMsg);
		}
	}
	
	public void r(String msg) {
		String printMsg = getMessagePrefix() + "Important: " + msg;
					
		try {
			print(_resultsFileWriter, printMsg);
		} catch (IOException e) {
			System.out.println(getMessagePrefix() + "Severe Error: Could not write results file.");
			e.printStackTrace();
		}
		
		if (_debugMode) {
			System.out.println(printMsg);
		}
	}
	
	/**
	 * Prepares the log for a new experiment.
	 * This method must be called each time a new experiment is
	 * going to be executed.
	 * 
	 * @param source Name of the node that sends the message.
	 * @param destination Name of the node that receives the message.
	 * @param deadline Deadline of the message.
	 * @param algorithm Algorithm used.
	 */
	public void startNewExperiment(String source, String destination, float deadline, String algorithm) {
		_source = source;
		_destination = destination;
		_deadline = deadline;
		_algorithm = algorithm;
		_ackTime = -1;
		_messagesCounter = 0;
		_jiterChannelsUsed =  new LinkedList<IJiterChannel>();
	}
	
	/**
	 * Writes the results on the results file.
	 */
	public void stopExperiment(int resultCode) {
		
		// For some reason we still have to check the result here.
		// Sometimes, the result given was 0, but ackTime was bigger than deadline.
		if ( (resultCode == 0) && (_ackTime > _deadline) ) {
			resultCode = 1;
		}
		
		String msg = getMessagePrefix() + "Result: " + resultCode + " " + _source + " " + _destination + " " + _deadline + " " + _algorithm +
				" " + _ackTime + " " + _messagesCounter;
		
		if (_algorithm.equalsIgnoreCase("J0") || _algorithm.equalsIgnoreCase("J1")) {
			int denominator = Integer.parseInt(_algorithm.substring(1)) + 1;
			int jiterTriesCounter = (_messagesCounter / denominator) - 1;
			
			String suffix = " " + jiterTriesCounter + " " + _jiterChannelsUsed.toString();
			msg += suffix;
		}
		
		try {
			print(_resultsFileWriter, msg);
			_resultsFileWriter.flush();
			// just for debug purposes everything will be written to debug file.
			print(_debugFileWriter, msg);
		} catch (IOException e) {
			System.out.println(getMessagePrefix() + "Severe Error: Could not write results file.");
			e.printStackTrace();
		}
		
		if (_debugMode) {
			System.out.println(msg);
		}
		
		_source = "not-set-yet";
		_destination = "not-set-yet";
		_deadline = 0;
		_algorithm = "not-set-yet";
		_ackTime = -1;
		_messagesCounter = 0;
		_jiterChannelsUsed =  new LinkedList<IJiterChannel>();
	}
	
	public void stopLog() {
		try {
			if (_errorsOnFile) _errorsFileWriter.close();
			if (_importantsOnFile) _importantsFileWriter.close();
			_debugFileWriter.close();
			_resultsFileWriter.close();
		} catch (IOException e) {
			System.out.println(getMessagePrefix() + "Severe Error: Could not close the required files.");
			e.printStackTrace();
		}
	}
	
	/**
	 * Increases the messages exchanged counter.
	 */
	public void countNewMessage() {
		_messagesCounter++;
	}
	
	/**
	 * Sets the time needed to receive the ACK.
	 * 
	 * @param ackTime The time needed to get the ACK.
	 */
	public void setAckTime(float ackTime) {
		if (_ackTime == -1) {
			_ackTime = ackTime;
		}
	}
	
	/**
	 * Adds a channel to the list of used channels list (jiter algorithm only).
	 * @param channel
	 */
	public void addNewJiterChannelUsed(IJiterChannel channel) {
		_jiterChannelsUsed.add(channel);
	}

	/**
	 * Returns the prefix of the log messages.
	 * Contains the current time and algorithm.
	 * 
	 * @return The prefix of the log messages.
	 */
	private String getMessagePrefix() {
		return Utils.currentTime() + ": Log: " + _algorithm + ": ";
	}
	
	/**
	 * Returns the name of the file.
	 * NOTE: This method was conceived taking into account that
	 * this application will run on Emulab. It assumes that
	 * the address given by emulab is: name-of-computer.name-of-experiment-.ciptest.emulab.net,
	 * where name-of-computer has 5 digits (example: nodea).
	 * 
	 * @param filePath
	 * @return
	 * @throws UnknownHostException
	 */
	private String getLogFileName(String filePath) throws UnknownHostException {
		String computerName = InetAddress.getLocalHost().getHostName();
		return filePath + computerName + Utils.currentTime() + ".txt";		
	}
	
	private void print(Writer w, String msg) throws IOException {
		w.write(msg + '\n');
	}
}
