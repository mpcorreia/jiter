package jiter.misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.jgroups.Address;
import org.jgroups.util.PayloadUUID;


/**
 * Class that provides a bunch of useful methods which are used
 * in the rest of the application.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @author Rui Silva
 * @version 1.0
 *
 */
public class Utils {
	
	/** A random numbers generator. */
    private static final Random random = new Random();
    
    /** The Runtime object associated with the application, */
    private static final Runtime runtime = Runtime.getRuntime();

    /**
     * Returns a random number between a given range.
     * 
     * @param min Minimum number of the range.
     * @param max Maximum number of the range.
     * @return A random number between a given range.
     */
    public static int getRandomInt(int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }

    public static <T> T getRandomListItem(List<T> list) {
        T item = null;
        int listSize = list.size();
        
        if (listSize == 0) {
        	return item;
        }
        
        int intendedIndex = getRandomInt(0, listSize - 1);

        try {
            item = list.get(intendedIndex);
        } catch (IndexOutOfBoundsException e) {
        	Log.getInstance().e("Can't return a random list item - index out of bounds.");
        } catch (Exception e) {
        	Log.getInstance().e("Can't return a random list item - exception caught:" + e.getMessage());
        }

        return item;
    }

    public static <T> List<T> getRandomSubList(List<T> list, int subListSize) {
        if (list.size() == subListSize) {
            return list;
        }

        List<T> randomList = new ArrayList<T>(list);
        Collections.shuffle(randomList);

        return randomList.subList(0, Math.min(subListSize, list.size()));
    }

    /**
     * Given an Address returns its logical name in a String.
     * 
     * @param address The Address whose logical name we want to get.
     * @return Logical Name in a String.
     */
    public static String addressToLogicalName(Address address) {
        return PayloadUUID.getContents().get(address);
    }

    /**
     * Given an Address returns its Node Id.
     * 
     * @param address The Address whose node id we want to get.
     * @return Node id.
     */
    public static String addressToNodeId(Address address) {
        return ((PayloadUUID) address).getPayload();
    }

    /**
     * Allows to run an UNIX command with given parameters and get
     * the output returned by the execution of the command.
     * 
     * @param command The command and parameters to run.
     * @return The output of the command.
     * @throws IOException
     */
    public static String[] runCommand(String command) throws IOException {
        List<String> lines = new LinkedList<String>();
        Process process = runtime.exec(command);

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;

        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }

        return lines.toArray(new String[0]);
    }

    /**
     * Given an array of objects and a glue elements, returns the
     * toString representation of the objects "glued" by the element passed
     * as argument.
     * 
     * @param parts Objects whose string representation we want to glue.
     * @param glue What connects the string representation of the objects.
     * @return The string representation of the objects, connected by the glue element.
     */
    public static <T> String joinString(T[] parts, String glue) {
        return joinString(parts, glue, new Stringer<T>() {
            public String string(T data) {
                return data.toString();
            }
        });
    }
    
    public static <T> String joinString(T[] parts, String glue, Stringer<T> stringer) {
        String result = "";

        for (int i = 0; i < parts.length - 1; i++) {
            result += stringer.string(parts[i]) + glue;
        }

        if (parts.length > 0) {
            result += stringer.string(parts[parts.length - 1]);
        }

        return result;
    }

    /**
     * Given a collection of objects and a glue elements, returns the
     * toString representation of the objects "glued" by the element passed
     * as argument.
     * 
     * @param parts Objects whose string representation we want to glue.
     * @param glue What connects the string representation of the objects.
     * @return The string representation of the objects, connected by the glue element.
     */
    public static <T> String joinString(Collection<T> parts, String glue) {
        return joinString(parts.toArray(), glue);
    }

    /**
     * Prints a collection of objects, using their string representation.
     * 
     * @param list of objects whose string representation we want to print.
     */
    public static <T> void printCollection(Collection<T> list) {
        printCollection(list, new Stringer<T>() {
            public String string(T data) {
                return data.toString();
            }
        });
    }

    /**
     * Prints a collection of objects, using their string representation.
     * 
     * @param list of objects whose string representation we want to print.
     * @param stringer object that defines the objects string representation.
     */
    public static <T> void printCollection(Collection<T> list, Stringer<T> stringer) {
        for (T element : list) {
            Log.getInstance().d(stringer.string(element));
        }
    }
    
    public static String currentTime() {
    	final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
    	
    	Calendar cal = Calendar.getInstance();
    	SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
    	return sdf.format(cal.getTime());
    }
    
    /**
     * Given an Exception, returns a String of its
     * stack trace.
     * 
     * @param e The exception whose stack trace string we want.
     * @return Exception's stack trace string.
     */
    public static String getStackTraceString(Exception e) {
    	StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	e.printStackTrace(pw);
    	
    	return sw.toString();
    }

    /**
     * This interface represents objects that given an object return
     * a string representation of that object.
     * 
	 * @author Alexandre Fonseca
	 * @author Pedro Marques da Luz
	 * @author Rui Silva
	 * @version 1.0
     *
     * @param <T> The object whose string representation we want.
     */
    public static interface Stringer<T> {
        public abstract String string(T data);
    }

    public static Field getField(Class clazz, String fieldName) throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
}
