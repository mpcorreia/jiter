package jiter.misc;

import java.io.IOException;

import java.net.InetAddress;

import java.util.LinkedList;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class executes the traceroute UNIX command and parses the output of it,
 * providing information about the RTT of a communication.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @author Rui Silva
 * @version 1.0
 *
 */
public class Traceroute {
    
	/** REGEX to parse the output of traceroute command. */
	private static final Pattern p = Pattern.compile("^\\d+\\s+(\\S+)\\s+(\\S+)\\s+ms$");
    
    /**
     * Executes the traceroute command and parses the results.
     * 
     * @param sourceIP of the communication.
     * @param destinationIP of the communication.
     * @return Array of nodes.
     * @throws IOException
     */
    public static Node[] run(String sourceIP, String destinationIP) throws IOException {
        List<Node> routeNodes = new LinkedList<Node>();

        sourceIP = stripPort(sourceIP);
        destinationIP = stripPort(destinationIP);

        String[] traceRouteOutput = Utils.runCommand("traceroute -n -q 1 -s " + sourceIP + " " + destinationIP);

        for (String line : traceRouteOutput) {
            Matcher m = p.matcher(line.trim());

            if (m.matches()) {
                String ipString = m.group(1);
                String rttString = m.group(2);
                InetAddress ip = InetAddress.getByName(ipString);
                float rtt = Float.parseFloat(rttString);
                routeNodes.add(new Node(ip, rtt));
            }
        }

        return routeNodes.toArray(new Node[0]);
    }

    /**
     * This class represents a node that exists in the communication between two points.
     * A node is defined by an IP a RTT.
     * 
     * @author Alexandre Fonseca
	 * @author Pedro Marques da Luz
	 * @author Rui Silva
	 * @version 1.0
     *
     */
    public static class Node {
    	
    	/** IP of the Node. */
        private InetAddress _ip;
        
        /** Measured RTT in a communication from a given source to the node. */
        private float _rtt;

        /**
         * Creates an instance of a Node.
         * 
         * @param ip of the node.
         * @param rtt in a communication from a given source to the node.
         */
        public Node(InetAddress ip, float rtt) {
            _ip = ip;
            _rtt = rtt;
        }

        /**
         * Returns the IP of the Node.
         * 
         * @return The IP of the Node.
         */
        public InetAddress getIp() {
            return _ip;
        }

        /**
         * Returns the RTT of the Node.
         * 
         * @return The RTT of the Node.
         */
        public float getRtt() {
            return _rtt;
        }
        
        public String toString() {
        	return _ip.toString() + " - " + _rtt + "ms";
        }
    }

    /**
     * Eliminates the port in a string containing an IP Address.
     * If the IP Address doesn't contain the port information,
     * the original string is returned.
     * 
     * @param ipAddress whose port we want to strip.
     * 
     * @return IP Address without the port information.
     */
    private static String stripPort(String ipAddress) {
        int portIndex = ipAddress.indexOf(':');

        if (portIndex != -1) {
            ipAddress = ipAddress.substring(0, portIndex);
        }

        return ipAddress;
    }
}
