package jiter;
import java.util.Map;
import org.jgroups.Address;
import java.util.TreeMap;

import jiter.misc.Utils;

/**
 * This class represents a Jiter message that correspond to messages that are sent and received between
 * Jiter nodes over normal IP network. 
 * A Jiter message is used to send data, acknowledgments, probe network RTT or to sync DC information.
 * To send this kind of information over an usual IP network specific header lines are added in the beginning
 * of IP data field that are converted in information when message received by a Jiter node.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @version 1.0
 */
public class JiterMessage {
	
	/** Represents the type of the message. */
	public enum Type {DATA, ACK, DCSYNC, TXT};

	/** Provides the id of the next message sent. */
    private static long nextId;
	
    /** Contains the values of the header fields of the message. */
	private Map<String, String> _headerLines;
	
	/** The data to be sent. */
	private String _data;
	
	/**
	 * Creates a Jiter message with next id available.
	 */
	public JiterMessage() {
        this(incrementId());
	}
	
	/**
	 * Creates a Jiter message with the specified id.
	 *
	 * @param id Id that specified for the creation of this jiter message.
	 * @return Return message id
	 */
    private JiterMessage(Long id) {
		_headerLines = new TreeMap<String, String>();
		_data = null;
        _headerLines.put("MessageId", Long.toString(id));
        setType(Type.DATA);
    }

	/**
	 * Obtains message id.
	 *
	 * @return Return message id
	 */
    public long getMessageId() {
        return Long.parseLong(_headerLines.get("MessageId"));
    }
	
	/**
	 * Obtains ACKs id.
	 *
	 * @return Return ack id
	 */
    public long getAckId() {
        return Long.parseLong(_headerLines.get("AckId"));
    }
	
	/**
	 * Obtains source's name of message.
	 *
	 * @return Return source's name
	 */
    public String getSourceName() {
        return _headerLines.get("SourceAddress");
    }
	
	/**
	 * Obtains destination's name of message.
	 *
	 * @return Return destination's name
	 */
	public String getDestinationName() {
        return _headerLines.get("DestinationAddress");
	}

	/**
	 * Obtains relay isp of message.
	 *
	 * @return Name of relay isp of message
	 */
    public String getRelayIsp() {
        return _headerLines.get("RelayIsp");
    }
	
	/**
	 * Obtains message deadline.
	 *
	 * @return Message deadline
	 */
	public String getDeadline() {
		return _headerLines.get("Deadline");
	}

	/** 
	 * Obtains message type.
	 *
	 * @return Message type or null if does not exist.
	 */
    public Type getType() {
        String strType = _headerLines.get("Type");

        if (strType == null) {
            return null;
        } else {
            return Enum.valueOf(Type.class, strType);
        }
    }

	/**
	 * Obtains message data.
	 *
	 * @return Message data
	 */
    public String getData() {
        return _data;
    }

	/**
	 * Define message source name.
	 */
    protected void setSourceName(String sourceName) {
        _headerLines.put("SourceAddress", sourceName);
    }

	/**
	 * Defines message destination name.
	 */
    protected void setDestinationName(String destName) {
        _headerLines.put("DestinationAddress", destName);
    }

	/**
	 * Defines message source address.
	 */
    public void setSourceAddress(Address sourceAddress) {
        _headerLines.put("SourceAddress", Utils.addressToLogicalName(sourceAddress));
    }

	/**
	 * Defines message destination address.
	 */
    public void setDestinationAddress(Address destAddress) {
        _headerLines.put("DestinationAddress", Utils.addressToLogicalName(destAddress));
    }

	/**
	 * Defines message relay isp.
	 */
    public void setRelayIsp(String relayIsp) {
        _headerLines.put("RelayIsp", relayIsp);
    }
	
	/**
	 * Defines message deadline.
	 */
	public void setDeadline(String string) {
		_headerLines.put("Deadline", string);
	}
	
	/**
	 * Defines message type.
	 */
    public void setType(Type type) {
        _headerLines.put("Type", type.toString());
    }

	/**
	 * Defines message data.
	 */
    public void setData(String data) {
        _data = data;
    }
	
	/**
	 * Creates an ack message for this message.
	 *
	 * @return Jiter message of type ack
	 */
    public JiterMessage getAckMessage() {
        JiterMessage ackMessage = new JiterMessage();
        ackMessage.setType(Type.ACK);
        ackMessage.setDestinationName(getSourceName());
        ackMessage.setSourceName(getDestinationName());
        ackMessage._headerLines.put("AckId", Long.toString(getMessageId()));

        return ackMessage;
    }

	/**
	 * Retrives a Jiter message information from give string.
	 *
	 * @param str String containing the jiter message
	 * @return Jiter message with information retrived
	 */
	public static JiterMessage getJiterMessageFromString(String str) throws NotAJiterMessageException {
		JiterMessage jiterMessage = new JiterMessage(0l);
		
		String string = new String(str);
		String[] lines = string.split("\n");

        if (lines.length == 0 || !lines[0].equalsIgnoreCase("JITER/1.0")) {
            throw new NotAJiterMessageException();
        }
		
		boolean analyzingHeaders = true;
		String data = "";
		
		for (String line: lines) {
			if (analyzingHeaders) {
                if (line.equals("")) {
                    analyzingHeaders = false;
                    continue;
                }

				String[] headerLineParts = line.split(":", 2);
	
				if (headerLineParts.length != 2) {
                    continue;
				}
				
				jiterMessage._headerLines.put(headerLineParts[0].trim(), headerLineParts[1].trim());
            } else {
				data += line + "\n";
            }
		}
		
		jiterMessage.setData(data);
		
		return jiterMessage;
	} 

	/**
	 * Creates a string from message.
	 *
	 * @return String created
	 */
    public String toString() {
        String headers = "JITER/1.0\n";

        for (Map.Entry<String, String> entry : _headerLines.entrySet()) {
			headers += entry.getKey() + ":" + entry.getValue() + "\n";
		}
		
		headers += "\n";

        return headers + _data;
	}
	
	/**
	 * Defines an exception that represents the event of receiving a message
	 * that it is not a Jiter Message.
	 */
    public static class NotAJiterMessageException extends Exception {
        static final long serialVersionUID = 1;

        public NotAJiterMessageException() {
            super("Supplied data does not contain a jiter message");
        }
    }

    private synchronized static long incrementId() {
        return nextId++;
    }
}
