package jiter;

import java.util.ArrayList;

import java.util.concurrent.Semaphore;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import org.jgroups.View;

import jiter.misc.Config;
import jiter.misc.Log;
import jiter.misc.Utils;

import jiter.strategies.FStrategy;
import jiter.strategies.SendStrategy;
import jiter.strategies.J0Strategy;
import jiter.strategies.J1Strategy;
import jiter.strategies.PBStrategy;

public class ExperimentNode implements JiterNode.IJiterNodeObserver {
    private static final int INITIAL_DELAY = 5000;
    private static final int INITIAL_DEADLINE = 250;  
    private static final int BETWEEN_DELAY = 2000;
    private static final int RESTART_DELAY = 30000;

    private static final int MINIMUM_NODES = 5;
    private JiterNode _jiterNode;
    private NodeIdList _nodeIdList;
    private ExperimentResult _currentExperimentResult;
    private String _activeNode;
    private int _activeNodeIndex = 0;
    private int _nodeDestinationIndex = 0;
    private List<SendStrategy> _strategies;
    private SendStrategy _currentStrategy;
    private ListIterator<SendStrategy> _strategyIterator;
    private JiterMessage _currentMessage;
    private Semaphore _startMessageReceiveSemaphore;
    private Semaphore _stopMessageReceiveSemaphore;
    private Semaphore _startMessageSendSemaphore;
    private Semaphore _stopMessageSendSemaphore;
    private List<Long> _startMessageIdsWait;
    private List<Long> _stopMessageIdsWait;
    private Log _log;
    private Map<String, Integer> _currentDeadlines;

    public static void main(String[] args) {
        try {
            ExperimentNode node = new ExperimentNode(args);
            node.run();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public ExperimentNode(String[] args) throws Exception {
        _log = Log.getInstance();
        _jiterNode = new JiterNode(args);
        _jiterNode.scanAndCreateSockets();
        _log.d("Starting local jiter node");
        _strategies = new ArrayList<SendStrategy>();
        _strategies.add(new J1Strategy(_jiterNode));
        _strategies.add(new J0Strategy(_jiterNode));
        _strategies.add(new PBStrategy(_jiterNode));
        _strategies.add(new FStrategy(_jiterNode));
        _strategyIterator = _strategies.listIterator();
        _startMessageIdsWait = new ArrayList<Long>();
        _stopMessageIdsWait = new ArrayList<Long>();
        _nodeIdList = new NodeIdList();
        _startMessageReceiveSemaphore = new Semaphore(0);
        _stopMessageReceiveSemaphore = new Semaphore(0);
        _currentDeadlines = new TreeMap<String, Integer>();
        for (SendStrategy s: _strategies) {
        	_currentDeadlines.put(s.toString(), INITIAL_DEADLINE);
		}
        
    }

    public void run() throws Exception {
        _jiterNode.registerObserver(this);
        _jiterNode.start();
        Config config = Config.getInstance();
        
        
        while(true) {
	        	
            _log.d("Waiting for " + MINIMUM_NODES + " nodes");
            // Wait for minimum number of nodes
            boolean waited = _nodeIdList.waitForNodes(MINIMUM_NODES);

            _log.d("NodeIdList:");
            _log.d(_nodeIdList.toString());

            // Check/Choose active node
            if (_activeNode == null) {
                _activeNode = _nodeIdList.get(_activeNodeIndex);
            }

            if (_activeNode.equals(_jiterNode.getId())) {
                _log.d("This node is the active node");

                if (waited) {
                    _log.d("Give some time for other nodes to completely setup");
                    //Thread.sleep(INITIAL_DELAY);
		    Thread.sleep(config.getTraceRouteUpdateInterval());
                } else {
                    _log.d("Wait for a bit before starting a new experiment");
                    Thread.sleep(BETWEEN_DELAY);
                }

                if (_nodeIdList.get(_nodeDestinationIndex).equals(_jiterNode.getId())) {
                    _log.d("Skipping node at index " + _nodeDestinationIndex);
                    _nodeDestinationIndex++;
                }

                if (_nodeDestinationIndex < _nodeIdList.getCount()) {
                    if (_currentStrategy == null) {
                        if (!_strategyIterator.hasNext()) {
                            _log.d("Resetting strategy iterator");
                            _strategyIterator = _strategies.listIterator();
                            Thread.sleep(RESTART_DELAY);
                        }
                        _currentStrategy = _strategyIterator.next();
                        _currentStrategy.initialize();
                        _log.d("Choosing strategy: " + _currentStrategy);
                        config.setSendStrategy(_currentStrategy);
                    }

                    // Send start experiment message
                    _log.i("Sending experiment start to other nodes");
                    sendStartMessage();

                    _currentExperimentResult = new ExperimentResult();
                    
                    // Send message
                    _currentMessage = new JiterMessage();
                    _currentMessage.setData(getExperimentMessageData());
                    String destination = _nodeIdList.get(_nodeDestinationIndex);
                    float deadline = _currentDeadlines.get(config.getSendStrategy().toString());
                    _log.startNewExperiment(_jiterNode.getLogicalNameFromNodeId(_activeNode), 
                    		_jiterNode.getLogicalNameFromNodeId(destination), 
                    		deadline, 
                    		config.getSendStrategy().toString());
                    _log.i("Sending experiment message to destination node: " + destination);
                    Config.getInstance().setExperimentMessageId(_currentMessage.getMessageId());
                    _jiterNode.send(_currentMessage, deadline, destination);
                    
                    // Wait for success/failure/retry
                    try {
                        _log.i("Waiting for experiment result");
                        _currentExperimentResult.waitForResult();
                    } catch (InterruptedException e) {
                    	_log.e(e.getMessage());
                    	_log.e(Utils.getStackTraceString(e));
                    }

                    _log.i("Experiment result: " + _currentExperimentResult.getResult());
                    _log.stopExperiment(_currentExperimentResult.getResult());
                }

                if (_currentExperimentResult.getResult() < 2) {
                    _log.d("Choosing next destination node");
                    _nodeDestinationIndex++;

                    if (_nodeDestinationIndex < _nodeIdList.getCount() &&
                        _nodeIdList.get(_nodeDestinationIndex).equals(_jiterNode.getId())) {
                        _log.d("Skipping node at index " + _nodeDestinationIndex);
                        _nodeDestinationIndex++;
                    }

                    if (_nodeDestinationIndex >= _nodeIdList.getCount()) {
                        _log.d("All destinations tested");
                        String currentStrategyName = config.getSendStrategy().toString();
                        _currentDeadlines.put(currentStrategyName, 
                        		getNextDeadline(_currentDeadlines.get(currentStrategyName)));
                        _nodeDestinationIndex = 0;
                        _log.d("Choosing next active node index");
                        _activeNodeIndex++;
                        _currentStrategy.dispose();
                        _currentStrategy = null;

                        if (_activeNodeIndex >= _nodeIdList.getCount()) {
                            _activeNodeIndex = 0;
                            _currentStrategy = null;
                        }

                        _log.d("Next active node index: " + _activeNodeIndex);
                        _activeNode = _nodeIdList.get(_activeNodeIndex);
                    }
                } else {
                    // We must repeat the current experiment
                    _log.i("Repeating current experiment");
                }

                // Send experiment end message (with next active if needed)
                _log.d("Sending experiment stop to other nodes");
                sendStopMessage();
            } else {
                // Wait for experiment start message
                _log.d("Waiting for start message from active node");
                waitReceiveStartMessage();

                // Wait for experiment end message
                _log.d("Waiting for stop message from active node");
                waitReceiveStopMessage();
            }
        }
    }

    public void sendStartMessage() {
        _startMessageIdsWait.clear();
        _startMessageSendSemaphore = new Semaphore(0);
        List<String> nodeIds = _nodeIdList.getNodeIds();
        int numberMessagesSent = 0;

        String data = "TEST\nSTART\n";
        // Add experiment data to message
        data += "STRATEGY=" + _currentStrategy.toString() + "\n";

        synchronized(_startMessageIdsWait) {
            for (String nodeId : nodeIds) {
                if (nodeId.equals(_jiterNode.getId())) {
                    continue;
                }

                // We recreate the messages so that they have different ids
                JiterMessage jiterMessage = new JiterMessage();
                jiterMessage.setData(data);

                try {
                    _log.d("Sending start message with id " + jiterMessage.getMessageId());
                    _jiterNode.sendTestMessage(jiterMessage, nodeId);
                    numberMessagesSent++;
                    _startMessageIdsWait.add(jiterMessage.getMessageId());
                } catch (Exception e) {
                	_log.e(e.getMessage());
                	_log.e(Utils.getStackTraceString(e));
                }
            }
        }

        _log.d("Waiting for start message acks");

        try {
            _startMessageSendSemaphore.acquire(numberMessagesSent);
        } catch (Exception e) {
        	_log.e(e.getMessage());
        	_log.e(Utils.getStackTraceString(e));
        }
    }

    public void sendStopMessage() {
        assert _stopMessageIdsWait != null : "stopMessageIds should not be null";
        assert _nodeIdList != null : "nodeIdList should not be null";
        assert _jiterNode != null : "jiterNode should not be null";

        _stopMessageIdsWait.clear();
        _stopMessageSendSemaphore = new Semaphore(0);
        List<String> nodeIds = _nodeIdList.getNodeIds();
        int numberMessagesSent = 0;

        String data = "TEST\nSTOP\n";
        // Add next master to message if needed
        if (!_activeNode.equals(_jiterNode.getId())) {
            data += "NEXT=" + _activeNode;
        }

        _log.d("Sending stop message with data:");
        _log.d(data);

        synchronized(_stopMessageIdsWait) {
            for (String nodeId : nodeIds) {
                if (nodeId.equals(_jiterNode.getId())) {
                    continue;
                }

                // We recreate the messages so that they have different ids
                JiterMessage jiterMessage = new JiterMessage();
                jiterMessage.setData(data);

                try {
                    _log.d("Sending stop message with id " + jiterMessage.getMessageId());
                    _jiterNode.sendTestMessage(jiterMessage, nodeId);
                    numberMessagesSent++;
                    _stopMessageIdsWait.add(jiterMessage.getMessageId());
                } catch (Exception e) {
                	_log.e(e.getMessage());
                	_log.e(Utils.getStackTraceString(e));
                }
            }
        }

        try {
            _stopMessageSendSemaphore.acquire(numberMessagesSent);
        } catch (Exception e) {
        	_log.e(e.getMessage());
        	_log.e(Utils.getStackTraceString(e));
        }
    }

    public void waitReceiveStartMessage() {
        try {
            _startMessageReceiveSemaphore.acquire();
        } catch (Exception e) {
        	_log.e(e.getMessage());
        	_log.e(Utils.getStackTraceString(e));
        }
    }

    public void waitReceiveStopMessage() {
        try {
            _stopMessageReceiveSemaphore.acquire();
        } catch (Exception e) {
        	_log.e(e.getMessage());
        	_log.e(Utils.getStackTraceString(e));
        }
    }

    public void onSendSuccessful(long messageId) {
        Long messageIdLong = new Long(messageId);
        
        if (_currentExperimentResult != null &&
            _currentMessage != null &&
            messageId == _currentMessage.getMessageId()) {
            _currentExperimentResult.setResult(0);
            return;
        }

        synchronized(_startMessageIdsWait) {
            if (_startMessageIdsWait.contains(messageIdLong)) {
                _log.d("Start message sent successfuly");
                _startMessageIdsWait.remove(messageIdLong);
                _startMessageSendSemaphore.release();
                return;
            }
        }

        synchronized(_stopMessageIdsWait) {
            if (_stopMessageIdsWait.contains(messageIdLong)) {
                _log.d("Stop message sent successfuly");
                _stopMessageIdsWait.remove(messageIdLong);
                _stopMessageSendSemaphore.release();
                return;
            }
        }
    }

    public void onSendFailed(long messageId) {
        if (_currentExperimentResult != null &&
            messageId == _currentMessage.getMessageId()) {
            _currentExperimentResult.setResult(1);
        }
    }

    public void onViewChanged(View view) {
        _nodeIdList.parseFromView(view);

        if (_nodeIdList.getCount() < MINIMUM_NODES && _currentExperimentResult != null) {
            _currentExperimentResult.setResult(2);
        }
    }

    public void onDataReceived(JiterMessage message) {
        String messageData = message.getData();
        String[] messageDataLines = messageData.split("\n");

        if (messageDataLines.length == 0) {
            _log.d("Received empty message");
            return;
        }

        if (messageDataLines[0].equals("TEST")) {
            if (messageDataLines.length == 1) {
                _log.d("Received incomplete test message");
                return;
            }

            if (messageDataLines[1].equals("START")) {
                if (messageDataLines.length == 2) {
                    _log.d("Missing strategy on test start message");
                    return;
                }
                String[] strategyLineParts = messageDataLines[2].split("=");
                String currentStrategyString = strategyLineParts[1];
                try {
                    Config.getInstance().setSendStrategy(currentStrategyString);
                    SendStrategy strategy = Config.getInstance().getSendStrategy();
                    try {
                        strategy.initialize();
                    } catch (Exception e) {
                    	_log.e(e.getMessage());
                    	_log.e(Utils.getStackTraceString(e));
                        System.exit(1);
                    }
                } catch (Exception e) {
                	_log.e(e.getMessage());
                	_log.e(Utils.getStackTraceString(e));
                    System.exit(1);
                }

                _startMessageReceiveSemaphore.release();
            }
            else if (messageDataLines[1].equals("STOP")) {
                if (messageDataLines.length == 3) {
                    String[] nextLineParts = messageDataLines[2].split("=");
                    _activeNode = nextLineParts[1];
                    _activeNodeIndex = _nodeIdList.getIndex(_activeNode);
                    _log.d("Detected new active node: " + _activeNode + "@" + _activeNodeIndex);
                    
                    if (_activeNode.equals(_jiterNode.getId())) {
                    	_log.r("DCMAT:\n" + _jiterNode.getDirectChannelMatrix().toString());
                    }
                }

                SendStrategy strategy = Config.getInstance().getSendStrategy();
                try {
                    strategy.dispose();
                } catch (Exception e) {
                	_log.e(e.getMessage());
                	_log.e(Utils.getStackTraceString(e));
                }
                _stopMessageReceiveSemaphore.release();
            }
        }
    }
    
    private String getExperimentMessageData() {
    	String base = "EXPERIMENT MESSAGE!EXPERIMENT MESSAGE!EXPERIMENT MESSAGE!EXPERIMENT MESSAGE!EXPERIMENT MESSAGE!";
    	String msg = "";
    	
    	// 1000 chars
    	for (int i = 1; i <= 10; i++) {
    		msg += base;
    	}
    	
    	return msg;
    }
    
    private int getNextDeadline(int d) {
    	
    	switch (d) {
			case INITIAL_DEADLINE: // 250ms
				d = 2 * INITIAL_DEADLINE;
				break;
			case (2 * INITIAL_DEADLINE): // 500ms
				d = 4 * INITIAL_DEADLINE;	
				break;
			case (4 * INITIAL_DEADLINE): // 1s
				d = 8 * INITIAL_DEADLINE;
				break;
			case (8 * INITIAL_DEADLINE): // 2s
				d = INITIAL_DEADLINE;
				break;
		}
    	
    	return d;
    }

    private static class ExperimentResult {
        // 0 -> Success
        // 1 -> Failed
        // 2 -> Repeat
        private Integer resultCode;
        private Object waitObject;

        public ExperimentResult() {
            resultCode = null;
            waitObject = new Object();
        }

        public void waitForResult() throws InterruptedException {
            synchronized(waitObject) {
                while (resultCode == null) {
                    waitObject.wait();
                }
            }
        }

        public int getResult() {
            return resultCode;
        }

        public void setResult(int resultCode) {
            this.resultCode = resultCode;
            synchronized(waitObject) {
                waitObject.notifyAll();
            }
        }
    }
}
