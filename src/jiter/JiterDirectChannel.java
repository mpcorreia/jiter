package jiter;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import jiter.misc.Config;
import jiter.misc.Log;
import jiter.misc.MovingAverageHelper;
import jiter.misc.Traceroute;
import jiter.misc.Utils;

import org.jgroups.Address;
import org.jgroups.stack.IpAddress;

/**
 * This class implements a jiter direct channel which keeps information about source, destination
 * and properties of a channel.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @author Rui Silva
 * @version 1.0
 */
public class JiterDirectChannel implements IJiterChannel, Serializable {
    
	/** Serial UID. */
	private static final long serialVersionUID = 1;

    /** Address of the source node of the channel. */
    private Address _source;
    
    /** Address of the destination node of the channel. */
    private Address _destination;
    
    /** Name of the ISP that provides the channel. */
    private String _isp;
    
    /** List of InetAddresses of the routers that compose the channel. */
    private List<InetAddress> _route;
    
    /** Last transmission time measured (in milliseconds). */
    private float _txt;
    
    /** Date of the last transmission time measurement update.*/
    private Calendar _lastTxtUpdate;
    
    /** Date of the last route update. */
    private Date _lastRouteUpdate;

    /** Timer that schedules the route update. */
    private transient Timer _routeUpdateTimer;
    
    /** Jiter Socket of the source. */
    private transient JiterSocket _sourceSocket;
    
    /** Moving Average Helper used to update txt values. */
    private transient MovingAverageHelper _movingAverageHelper;
    
    /** Timer that schedules the ping pong update. */
    private transient Timer _pingPongUpdateTimer;

    /** Associated DC Matrix */
    private transient DirectChannelMatrix _dcMatrix;

	/**
     * Creates a JiterDirectChannel.
     * 
     * @param source Channel source node
     * @param destination Channel destination node
     * @param isp Channel isp
     */
    public JiterDirectChannel(JiterSocket sourceSocket, Address destination) {
        _sourceSocket = sourceSocket;
        _source = sourceSocket.getAddress();
        _isp = sourceSocket.getIsp();
        _destination = destination;
        _route = new LinkedList<InetAddress>();
        _txt = -1;
        _lastTxtUpdate = Calendar.getInstance();
        _lastRouteUpdate = new Date(0);
        _routeUpdateTimer = new Timer(true);
        _pingPongUpdateTimer = new Timer(true);

        Config config = Config.getInstance();

        _movingAverageHelper = new MovingAverageHelper(config.getMovingAverageSamples());

        // Randomize starting time of the trace route and ping pong update to distribute 
        // the network stress these operation through time.
        int startingTime = Utils.getRandomInt(0, config.getTraceRouteUpdateInterval());
        _routeUpdateTimer.schedule(this.new TraceRouteHandlerTask(), startingTime, config.getTraceRouteUpdateInterval());
        
        startingTime = Utils.getRandomInt(0, config.getTxtUpdateInterval());
        _pingPongUpdateTimer.schedule(new PingPongHandlerTask(), startingTime, config.getTxtUpdateInterval());

        _dcMatrix = null;
    }

	/**
     * Obtains source's address of channel
     * 
     * @return Source's address
     */
    public Address getSource() {
        return _source;
    }

    public String getSourceNodeId() {
        return Utils.addressToNodeId(_source);
    }

	/**
     * Obtains relay's address of channel
     * 
     * @return Relay's address
     */
    public Address getRelay() {
        return null;
    }

    /**
     * Returns the Id of the relay node of the channel.
     * 
     * @return The Id of the relay node of the channel.
     */
    public String getRelayNodeId() {
        return null;
    }

	/**
     * Obtains destination's address of channel
     * 
     * @return Destionation's address
     */
    public Address getDestination() {
        return _destination;
    }

    /**
     * Returns the Id of the destination node of the channel.
     * 
     * @return The Id of the relay node of the channel. 
     */
    public String getDestinationNodeId() {
        return Utils.addressToNodeId(_destination);
    }

	/**
     * Obtains isps used by channel
     * 
     * @return List of ips used.
     */
    public List<String> getIsps() {
        List<String> isps = new LinkedList<String>();
        isps.add(_isp);
        return isps;
    }

	/**
     * Obtains route that compose channel
     * 
     * @return List of address that compose channels's route
     */
    public List<InetAddress> getRoute() {
        return new LinkedList<InetAddress>(_route);
    }

	/**
     * Obtains estimated txt for channel.
     * 
     * @return Estimated txt.
     */
    public float getTxt() {
        return _txt;
    }

	/**
     * Obtains the date of last txt update.
     * 
     * @return Date of last txt update
     */
    public Calendar getLastTxtUpdate() {
        return _lastTxtUpdate;
    }

	/**
     * Obtains the date of last route update.
     * 
     * @return Date of last route update
     */
    public Date getLastRouteUpdate() {
        return _lastRouteUpdate;
    }

	/**
     * Defines channel's route
     * 
     * @param route List of address that compose route
     */
    public void setRoute(List<InetAddress> route) {
        _lastRouteUpdate = new Date();

        if (_route != null && !_route.equals(route)) {
            /*System.out.println("Route has changed in " + toString() + ":");
            System.out.println("Previous: " + Utils.joinString(_route, ","));
            System.out.println("Now: " + Utils.joinString(route, ","));*/
            notifyChanged();
        }

        _route = route;
    }

	/**
     * Defines channel's estimated txt.
     * 
     * @param txt Estimated txt
     */
    public void setTxt(float txt) {
        _lastTxtUpdate = Calendar.getInstance();

        if (_txt != txt) {
            /*System.out.println("TXT has changed in " + toString() + ":");
            System.out.println("Previous: " + _txt);
            System.out.println("Now: " + txt);*/
            notifyChanged();
        }

        _txt = txt;
    }
    
	public void updateChannelTxt(double txt) {
		_lastTxtUpdate = Calendar.getInstance();
		_movingAverageHelper.add(txt);
		setTxt(_movingAverageHelper.getAverage());
	}
	
	/**
     * Prints channel to a formated string.
	 *
	 * @return Formated string with channel information.
     */
    public String toString() {
        return "[JiterDirectChannel " + Utils.addressToLogicalName(getSource()) + 
        		" | " + 
        		Utils.addressToLogicalName(getDestination()) + 
        		" isp: " + _isp + " txt: " + _txt + "]";
    }

    public String toDetailedString() {
        return "JiterDirectChannel {\n" +
            "\tSource: " + Utils.addressToLogicalName(getSource()) + " (NodeID: " + getSourceNodeId() + ")\n" + 
            "\tDestination: " + Utils.addressToLogicalName(getDestination()) + " (NodeID: " + getDestinationNodeId() + ")\n" + 
            "\tRoute: [" + Utils.joinString(getRoute(), ", ") + "]\n" +
            "\tTxt: " + getTxt() + "\n" + 
            "}";
    }

    public void setAssociatedDCMatrix(DirectChannelMatrix matrix) {
        _dcMatrix = matrix;
    }

    protected void notifyChanged() {
        if (_dcMatrix != null) {
            _dcMatrix.associatedChannelWasUpdated(this);
        }
    }

    /**
     * This class encapsulates the procedures to be done periodically to update
     * the route (of routers) and the transmission time measurement of the channel.
     * For this purpose is used the Unix tool Traceroute.
     * 
	 * @author Alexandre Fonseca
     * @author Pedro Marques da Luz
     * @author Rui Silva
     * @version 1.0
     */
    private class TraceRouteHandlerTask extends TimerTask {
    	
    	/**
    	 * Runs the necessary procedures to update the route and the transmission
    	 * time measurement of the channel.
    	 */
        public void run() {
            if (_sourceSocket == null) {
                cancel();
                return;
            }

            InetAddress sourceIp = _sourceSocket.getBindAddress();
            IpAddress destinationIp = _sourceSocket.getIpAddressFromAddress(getDestination());

            // If any of the ips are null, this usually means that this JiterDirectChannel
            // is no longer being used. Since Java lacks destructors and uses garbage collection, 
            // we cannot call timer cancel upon losing the last reference. In alternative to this,
            // one could implement a 'close' method that would be called everytime a jiterchannel
            // was deemed as no longer being needed.
            if (sourceIp ==  null 
            		|| destinationIp == null 
            		|| !_sourceSocket.getNode().isRegisteredRemoteAddress(getDestination())) {
                //System.out.println("Cancelling route thread");
                cancel();
                return;
            }

            try {
                Traceroute.Node[] routeNodes = Traceroute.run(sourceIp.getHostAddress(), destinationIp.toString());
                Log.getInstance().i("TR: Ran traceroute on " + JiterDirectChannel.this.toString() + ". Found " + routeNodes.length + " nodes.");
                List<InetAddress> route = new LinkedList<InetAddress>();

                for (Traceroute.Node node : routeNodes) {
                    route.add(node.getIp());
                }

                Log.getInstance().i("TR: " + Utils.joinString(route, ","));
                setRoute(route);

                if (routeNodes.length > 0) {
                	// the last node of the array is a jiter node
                    updateChannelTxt(routeNodes[routeNodes.length - 1].getRtt() / 2);
                }
            } catch (IOException e) {
                Log.getInstance().e("Failed to update traceroute on channel " + JiterDirectChannel.this.toString());
                Log.getInstance().e(Utils.getStackTraceString(e));
            }
        }
    }
    
    
    private class PingPongHandlerTask extends TimerTask {

		@Override
		public void run() {
            if (_dcMatrix != null) {
                float actualMilliseconds = Calendar.getInstance().getTimeInMillis();
                float lastTxtUpdateMilliseconds = _lastTxtUpdate.getTimeInMillis();
                float millisecondsPassed = actualMilliseconds - lastTxtUpdateMilliseconds;
                
                if (millisecondsPassed > Config.getInstance().getMaximumNoTxtUpdateInterval()) {
                    _sourceSocket.doPingPongUpdate(JiterDirectChannel.this);
                }
            } else {
                //System.out.println("Cancelling txt thread");
                cancel();
            }
		}
    }
}
