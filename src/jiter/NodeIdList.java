package jiter;

import java.lang.InterruptedException;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.jgroups.Address;
import org.jgroups.View;

import jiter.misc.Log;
import jiter.misc.Utils;

public class NodeIdList {
    private TreeMap<Integer, Object> waitObjects;
    private List<String> nodeIds;

    public NodeIdList() {
        waitObjects = new TreeMap<Integer, Object>();
        nodeIds = new LinkedList<String>();
    }

    public void parseFromView(View view) {
        synchronized(nodeIds) {
            nodeIds.clear();

            Set<String> nodeIdSet = new HashSet<String>();

            for (Address socketAddress : view) {
                nodeIdSet.add(Utils.addressToNodeId(socketAddress));
            }

            nodeIds.addAll(nodeIdSet);
            Collections.sort(nodeIds);

            Map<Integer, Object> objectsToNotifyMap = waitObjects.subMap(1, true, nodeIds.size(), true);

            for (Object waitObject : objectsToNotifyMap.values()) {
                Log.getInstance().d("Notifying wait object");
                synchronized(waitObject) {
                    waitObject.notifyAll();
                }
            }
        }
    }

    public String get(int index) {
        synchronized(nodeIds) {
            return nodeIds.get(index);
        }
    }

    public int getIndex(String nodeId) {
        synchronized(nodeIds) {
            return nodeIds.indexOf(nodeId);
        }
    }

    public int getCount() {
        synchronized(nodeIds) {
            return nodeIds.size();
        }
    }

    public List<String> getNodeIds() {
        synchronized(nodeIds) {
            return Collections.unmodifiableList(nodeIds);
        }
    }

    public boolean waitForNodes(int numberOfNodes) {
        boolean waited = false;
        while (getCount() < numberOfNodes) {
            try {
                Object waitObject = waitObjects.get(numberOfNodes);
                if (waitObject == null) {
                    waitObject = new Object();
                    waitObjects.put(numberOfNodes, waitObject);
                }
                synchronized(waitObject) {
                    waited = true;
                    waitObject.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return waited;
    }

    @Override
    public String toString() {
        return Utils.joinString(getNodeIds(), "\n");
    }
}

