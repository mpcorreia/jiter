package jiter;

import java.util.List;

import org.jgroups.Address;
import java.net.InetAddress;

/**
 * This interface represents a generic channel.
 * A channel might be direct or indirect.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @author Rui Silva
 * @version 1.0
 *
 */
public interface IJiterChannel {
	
	/**
	 * Returns the Address of the source node of the channel.
	 * 
	 * @return The Address of the source node of the channel.
	 */
	public Address getSource();
	
	/**
	 * Returns the Id of the source node of the channel.
	 * 
	 * @return The Id of the source node of the channel.
	 */
    public String getSourceNodeId();
    
    /**
     * Returns the Address of the relay node of the channel.
     * 
     * @return The Address of the relay node of the channel.
     */
    public Address getRelay();
    
	/**
	 * Returns the Id of the relay node of the channel.
	 * 
	 * @return The Id of the relay node of the channel.
	 */
    public String getRelayNodeId();
    
    /**
	 * Returns the Address of the destination node of the channel.
	 * 
	 * @return The Address of the destination node of the channel.
	 */
	public Address getDestination();
	
	/**
	 * Returns the Id of the destination node of the channel.
	 * 
	 * @return The Id of the destination node of the channel.
	 */
    public String getDestinationNodeId();
    
    /**
     * Returns the list of the ISPs of the direct channels that compose
     * the channel.
     * 
     * @return The list of the ISPs of the direct channels that compose
     * the channel.
     */
	public List<String> getIsps();
	
	/**
	 * Returns the list of InetAddresses of the routers of the channel.
	 * 
	 * @return The list of InetAddresses of the routers of the channel.
	 */
	public List<InetAddress> getRoute();
	
	/**
	 * Returns the last transmission time measured.
	 * 
	 * @return The last transmission time measured.
	 */
	public float getTxt();
	
	public String toDetailedString();
}
