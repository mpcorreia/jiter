package jiter;

import java.io.BufferedReader;

import java.util.List;

import org.jgroups.Address;
import java.io.InputStreamReader;

import org.jgroups.stack.IpAddress;
import java.util.Collection;

import jiter.misc.Config;
import jiter.misc.Utils;

public class MainTest {
    /*public static void main(String[] args) {
        JiterNode jiterNode = null;

        try {
            jiterNode = new JiterNode(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        List<JiterSocket> localSockets;

        try {
            jiterNode.scanAndCreateSockets();

            System.out.println("Starting local jiter node");
            jiterNode.start();
            localSockets = jiterNode.getLocalSockets();

            System.out.println("");
            System.out.println("Available commands: ");
            System.out.println("--------------------");
            System.out.println("send       - Send a message");
            System.out.println("listremote - Lists all known remote jiter nodes");
            System.out.println("listlocal  - Lists all local jiter nodes");
            System.out.println("showdc     - Show DC matrix");
            System.out.println("getip      - Gets the IP and port associated with a node name");
            System.out.println("select     - Selects one of the local nodes");
            System.out.println("quit       - Close the node");
            System.out.println("");
            System.out.print("> ");

            String strRead = "";

            while ((strRead = stdin.readLine()) != null) {
                try {
                    strRead = strRead.trim();

                    if (strRead.equalsIgnoreCase("send")) {
                        Address destinationAddress = null;
                        Address relayAddress = null;
                        String relayIsp = null;

                        do {
                            System.out.print("Destination Name: ");
                            destinationAddress = jiterNode.getAddressFromName(stdin.readLine());
                        } while (destinationAddress == null);

                        System.out.print("Relay Name: ");
                        relayAddress = jiterNode.getAddressFromName(stdin.readLine());

                        if (relayAddress != null) {
                            System.out.print("Relay Isp: ");
                            relayIsp = stdin.readLine().trim();
                            if (relayIsp.equals("")) {
                                relayIsp = null;
                            }
                        }

                        System.out.println("Message (terminate with // on a new line):");
                        String messageData = "";
                        String msgLineRead = "";

                        while ((msgLineRead = stdin.readLine()) != null) {
                            if (msgLineRead.equals("//")) {
                                break;
                            }

                            messageData += msgLineRead;
                        }

                        JiterMessage jiterMessage = new JiterMessage();
                        jiterMessage.setDestinationAddress(destinationAddress);
                        jiterMessage.setData(messageData);
                        if (relayIsp != null) {
                            jiterMessage.setRelayIsp(relayIsp);
                        }
                        
                        if (relayAddress == null) {
                            selectedJiterSocket.sendMessage(destinationAddress, jiterMessage);
                        } else {
                            selectedJiterSocket.sendMessage(relayAddress, jiterMessage);
                        }

                        System.out.println("Message sent!");
                    }
                    else if (strRead.equalsIgnoreCase("listremote")) {
                        System.out.println("Known remote jiter sockets:");
                        Utils.printCollection(jiterNode.getKnownRemoteSocketAddresses(), new Utils.Stringer<Address>() {
                            public String string(Address data) {
                                return Utils.addressToLogicalName(data) + " (NodeID: " + Utils.addressToNodeId(data) + ")";
                            }
                        });
                    }
                    else if (strRead.equalsIgnoreCase("listlocal")) {
                        System.out.println("Local jiter sockets:");
                        Utils.printCollection(jiterNode.getLocalSockets());
                    }
                    else if (strRead.equalsIgnoreCase("showdc")) {
                        System.out.println(jiterNode.getDirectChannelMatrix());
                    }
                    else if (strRead.equalsIgnoreCase("getip")) {
                        System.out.print("Socket name: ");
                        String nodeName = stdin.readLine().trim();

                        IpAddress ipAddress = selectedJiterSocket.getIpAddressFromAddress(jiterNode.getAddressFromName(nodeName));

                        if (ipAddress == null) {
                            System.out.println("No ip address found");
                        } else {
                            System.out.println("Ip address: " + ipAddress);
                        }
                    }
                    else if (strRead.equalsIgnoreCase("select")) {
                        System.out.print("Local socket isp: ");
                        String strIsp = stdin.readLine().trim();
                        try {
                            selectedJiterSocket = jiterNode.getLocalSocket(strIsp);
                            System.out.println("Selected socket " + selectedJiterSocket);
                        } catch (Exception e) {
                        }
                    }
                    else if (strRead.equalsIgnoreCase("quit")) {
                        break;
                    }

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                System.out.println("");
                System.out.println("");
                System.out.print("> ");
            }

            System.out.println("Stopping jiter node...");
            jiterNode.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static <T> void printCollectionWithLineNumbers(Collection<T> list, final int startingLineNumber) {
        Utils.printCollection(list, new Utils.Stringer<T>() {
            private int lineNumber = startingLineNumber - 1;

            public String string(T data) {
                lineNumber++;
                return lineNumber + " - " + data;
            }
        });
    }

    */
}
