package jiter;

import java.util.List;
import java.util.LinkedList;

import org.jgroups.Address;
import java.net.InetAddress;

import jiter.misc.Utils;

/**
 * This class implements a jiter indirect channel which keeps information about source, destination
 * and properties of a channel.
 * In this context, a jiter indirect channel is composed by two jiter direct channels.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @version 1.0
 */
public class JiterIndirectChannel implements IJiterChannel {
	
	/** First jiter direct channel that composes the indirect channel. */
    JiterDirectChannel _channel1;
    
    /** Second jiter direct channel that composes the indirect channel. */
    JiterDirectChannel _channel2;
    
    /**
     * Creates a jitter indirect channel using two jiter direct channels.
     * 
     * @param chan1 First jiter direct channel that composes the indirect channel.
     * @param chan2 Second jiter direct channel that composes the indirect channel.
     */
    public JiterIndirectChannel(JiterDirectChannel chan1, JiterDirectChannel chan2) {
        _channel1 = chan1;
        _channel2 = chan2;
    }

    /**
     * Returns the Address of the source node of the channel.
     * 
     * @return The Address of the source node of the channel.
     */
    public Address getSource() {
        return _channel1.getSource();
    }

    /**
     * Returns the Id of the source node of the channel.
     * 
     * @return The Id of the source node of the channel.
     */
    public String getSourceNodeId() {
        return _channel1.getSourceNodeId();
    }

    /**
     * Returns the Address of the relay node of the channel.
     * 
     * @return The Address of the relay node of the channel.
     */
    public Address getRelay() {
        return _channel1.getDestination();
    }

    /**
     * Returns the Id of the relay node of the channel.
     * 
     * @return The Id of the relay node of the channel.
     */
    public String getRelayNodeId() {
        return _channel1.getDestinationNodeId();
    }

    /**
     * Returns the Address of the destination node of the channel.
     * 
     * @return The Address of the destination node of the channel.
     */
    public Address getDestination() {
        return _channel2.getDestination();
    }

    /**
     * Returns the Id of the destination node of the channel.
     * 
     * @return The Id of the destination node of the channel.
     */
    public String getDestinationNodeId() {
        return _channel2.getDestinationNodeId();
    }

    /**
     * Returns a list with the names of the ISPs of the two
     * jiter direct channels that compose the indirect channel.
     * 
     * @return The ISPs of the two jiter direct channels that compose the indirect channel.
     */
    public List<String> getIsps() {
        List<String> isps = new LinkedList<String>();
        isps.addAll(_channel1.getIsps());
        isps.addAll(_channel2.getIsps());
        return  isps;
    }

    /**
     * Returns a list with the InetAddresses of the routers that compose the route
     * of this channel.
     * 
     * @return A list with the InetAddresses of the routers that compose the route
     * of this channel.
     */
    public List<InetAddress> getRoute() {
        List<InetAddress> route = new LinkedList<InetAddress>();
        route.addAll(_channel1.getRoute());
        route.addAll(_channel2.getRoute());

        return route;
    }

    /**
     * Returns the transmission time measurement of the channel.
     * 
     * @return The transmission time measurement of the channel.
     */
    public float getTxt() {
        return _channel1.getTxt() + _channel2.getTxt();
    }
    
    public String toDetailedString() {
        return "JiterIndirectChannel {\n" +
            "\tSource: " + Utils.addressToLogicalName(getSource()) + " (NodeID: " + getSourceNodeId() + ")\n" + 
            "\tDestination: " + Utils.addressToLogicalName(getDestination()) + " (NodeID: " + getDestinationNodeId() + ")\n" + 
            "\tRoute: [" + Utils.joinString(getRoute(), ", ") + "]\n" +
            "\tTxt: " + getTxt() + "\n" + 
            "}";
    }
    
    public String toString() {
    	return "[JiterIndirectChannel " + Utils.addressToLogicalName(getSource()) + 
        		" | " +
    			Utils.addressToLogicalName(getRelay()) + 
    			" | " +
        		Utils.addressToLogicalName(getDestination()) + 
        		" isp1: " + getIsps().get(0) + " isp2: " + getIsps().get(1) + " txt: " + getTxt() + "]";
    }
    
}
