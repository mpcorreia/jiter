package jiter;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import jiter.misc.Log;
import jiter.misc.Utils;

import org.jgroups.Address;

/**
 * This class represents a matrix containing all direct jiter channels known.
 * This class also extends from Observable allowing OC matrix to join as observer for modifications.
 * 
 * @author Alexandre Fonseca
 * @author Pedro Marques da Luz
 * @version 1.0
 *
 */
public class DirectChannelMatrix extends Observable implements Serializable {

	private static final long serialVersionUID = 3923030182695162700L;

	/** The node to which the DC matrix belongs. */
    private transient JiterNode _node;
    
    /** The map that stores the Jiter Direct Channels. */
    private Map<DirectChannelId, JiterDirectChannel> _dcMap;
    
	/** 
     * Creates an empty DC matrix for the specified node.
     * 
     * @param node JiterNode responsible for this DC matrix.
     */
    public DirectChannelMatrix(JiterNode node) {
        _node = node;
        _dcMap = new HashMap<DirectChannelId, JiterDirectChannel>();
    }
    
	/**
     * Obtains a direct jiter channel.
     * 
     * @param source Source of required channel
     * @param destination Destination of required channel
     * 
     * @return A direct channel with given specifications 
     */
    public synchronized JiterDirectChannel get(Address source, Address destination) {
        DirectChannelId id = new DirectChannelId(source, destination);
        return _dcMap.get(id);
    }
    
	/**
     * Adds a new local JiterDirectChannel to DC matrix.
     * 
     * @param channel JiterDirectChannel to add
     */
    public synchronized void addLocalDirectChannel(JiterDirectChannel channel) {
        // If this isn't a local direct channel, don't do anything
        if (_node.getLocalSocket(channel.getSource()) == null) {
            Log.getInstance().d("Tried to add a non local direct channel as a local direct channel");
            Log.getInstance().d(channel.toDetailedString());
            return;
        }

        channel.setAssociatedDCMatrix(this);
        add(channel);
        setChanged();
        notifyObservers();
    }

    public synchronized void add(JiterDirectChannel channel) {
        DirectChannelId id = new DirectChannelId(channel);
        _dcMap.put(id, channel);
    }

    /**
     * Removes all channels from this DC Matrix instance that contain references
     * to the specified socket address (either on source or destination).
     * 
     * @param socketAddress The address of the socket whose channels it participates 
     * in are to be removed.
     */
    public synchronized void removeChannelsWithSocketAddress(Address socketAddress) {
        Iterator<Map.Entry<DirectChannelId, JiterDirectChannel>> it = _dcMap.entrySet().iterator();
        boolean removedChannels = false;

        while (it.hasNext()) {
            Map.Entry<DirectChannelId, JiterDirectChannel> entry = it.next();
            DirectChannelId chanId = entry.getKey();

            if (chanId.getSource().equals(socketAddress) ||
                chanId.getDestination().equals(socketAddress)) {
                JiterDirectChannel channel = entry.getValue();
                channel.setAssociatedDCMatrix(null);
                removedChannels = true;
                it.remove();
            }
        }

        if (removedChannels) {
            setChanged();
            notifyObservers();
        }
    }
	
	/**
     * Retrieves all channels from DC matrix.
     * 
     * @return A collection with all channels from DC matrix
     */
    public synchronized Collection<JiterDirectChannel> getAllChannels() {
        return new LinkedList<JiterDirectChannel>(_dcMap.values());
    }
    
    /**
     * Gets a list of JiterDirectChannels that have the specified source node.
     *
     * Note: The returned entries may have multiple source addresses (when a
     * node is connected to more than one isp for instance).
     * 
     * @param source The id of the node that is a source of the channels we want.
     * @return A list of direct channels.
     */
    public synchronized List<JiterDirectChannel> getChannelsWithSourceNode(String sourceId) {
        List<JiterDirectChannel> channels = new LinkedList<JiterDirectChannel>();

        for (Map.Entry<DirectChannelId, JiterDirectChannel> entry : _dcMap.entrySet()) {
            DirectChannelId chanId = entry.getKey();

            if (chanId.getSourceNodeId().equals(sourceId)) {
                channels.add(entry.getValue());
            }
        }
        
        return channels;
    }

    public synchronized List<JiterDirectChannel> getChannelsWithDestinationNode(String destinationId) {
        List<JiterDirectChannel> channels = new LinkedList<JiterDirectChannel>();

        for (Map.Entry<DirectChannelId, JiterDirectChannel> entry : _dcMap.entrySet()) {
            DirectChannelId chanId = entry.getKey();

            if (chanId.getDestinationNodeId().equals(destinationId)) {
                channels.add(entry.getValue());
            }
        }
        
        return channels;
    }
    
    /**
     * Updates a DirectChannelMatrix instance with data received from a list of
     * channels.
     * 
     * @param channels A list of JiterDirectChannels containing the new values.
     */
    public synchronized void updateWithExternalData(List<JiterDirectChannel> channels) {
        for (JiterDirectChannel channel : channels) {
            DirectChannelId id = new DirectChannelId(channel);

            JiterDirectChannel existingChannel = _dcMap.get(id);

            if (existingChannel == null) {
                add(channel);
            } else {
                existingChannel.setRoute(channel.getRoute());
                existingChannel.setTxt(channel.getTxt());
            }
        }
        Log.getInstance().i("DCMAT:\n" + this.toString());
    }

	/**
     * Prints all channels to a formated string.
     * 
     * @return String with information about all channels.
     */
    public synchronized String toString() {
        String string = "DCMatrix of JiterNode '" + _node.getLogicalNameFromNodeId(_node.getId()) + "' {\n";

        for (JiterDirectChannel channel : _dcMap.values()) {
            string += channel.toDetailedString() + "\n";
        }

        string += "}";

        return string;
    }

    public synchronized void associatedChannelWasUpdated(IJiterChannel channel) {
        setChanged();
        notifyObservers();
    };
    
    /**
     * This class represents an Id of a Jiter Direct Channel.
     * The Id is composed by a source and destination Address as well as the name of
     * the ISP that provides the channel.
     * 
     * @author Alexandre Fonseca
     * @author Pedro Marques da Luz
     * @author Rui Silva
     * @version 1.0
     *
     */
    private static class DirectChannelId implements Serializable{
    	
		private static final long serialVersionUID = 397394705799643941L;

		/** Address of the source node. */
        private Address _source;
        
        /** Address of the destination node. */
        private Address _destination;
        
        /**
         * Creates a Direct Channel Id with the given the source and destination
         * addresses.
         * 
         * @param source Address of the source node.
         * @param destination Address of the destination node.
         */
        public DirectChannelId(Address source, Address destination) {
            _source = source;
            _destination = destination;
        }

        /**
         * Creates a Direct Channel Id using the information of a given Jiter Direct
         * Channel.
         * @param channel
         */
        public DirectChannelId(JiterDirectChannel channel) {
            this(channel.getSource(), channel.getDestination());
        }

        /**
         * Returns the Address of the source node.
         * 
         * @return The Address of the source node.
         */
        public Address getSource() {
            return _source;
        }

        /**
         * Returns the Id of the source node.
         * 
         * @return The Id of the source node.
         */
        public String getSourceNodeId() {
            return Utils.addressToNodeId(_source);
        }

        /**
         * Returns the Address of the destination node.
         * 
         * @return The Address of the destination node.
         */
        public Address getDestination() {
            return _destination;
        }

        /**
         * Returns the Id of the destination node.
         * 
         * @return The Id of the destination node.
         */
        public String getDestinationNodeId() {
            return Utils.addressToNodeId(_destination);
        }

        /**
         * Compares if a given object is a Direct Channel Id with the same
         * values of this one.
         * The values taken into account are the source and destination Address
         * as well as the ISP name.
         * 
         * @return 0 if the objects are equal. -1 or 1 otherwise.
         *
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (!(obj instanceof DirectChannelId)) return false;

            DirectChannelId id = (DirectChannelId) obj;
            return id._source.equals(_source) &&
                   id._destination.equals(_destination);
        }
        
        /**
         * Necessary in order for the hashMap to provide the right
         * object. The default hashCode method takes in account the
         * object reference. Since we are comparing the object
         * according to the value of its attributes, we have to override
         * this.
         * 
         * @return the hashcode.
         */
        @Override
        public int hashCode() {
        	// this should be reconsidered in order to get a better
        	// dispersion in the hash map.
        	return 0;
        }
    }
}
