package jiter.handlers;

import org.jgroups.Address;

import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocket;
import jiter.JiterSocketException;
import jiter.misc.Log;
import jiter.misc.Utils;

public class RelayHandler implements IMessageHandler {
    public boolean handleReceiveMessage(JiterSocket socket, JiterMessage message) {
        JiterNode node = socket.getNode();
        Address destinationAddress = node.getAddressFromName(message.getDestinationName());

        // if the node is not a relay of the channel
        if (destinationAddress == null || node.isLocalSocketAddress(destinationAddress) ||
        		node.isTestSocketAddress(destinationAddress)) {
        	Log.getInstance().d("RelayHandler: Skipping RelayHandler because the node isn't a relay.");
            return false;
        }

        JiterSocket relay = socket;

        // if we are sending through an ISP different from the one we received the
        // message, we need to change the sending socket.
        String relayIsp = message.getRelayIsp();

        if (relayIsp != null && !relayIsp.equals(relay.getIsp())) {
            relay = node.getLocalSocket(relayIsp);
        }

        if (relay != null) {
            try {
                /*System.out.println("* Forwarding jiter message to: " + destinationAddress);
                System.out.println("  Via: " + relay.getAddress());*/
                try {
                    //TODO: Don't forget to remove when testing
                    //Thread.sleep(1000);
                } catch (Exception e) {
                }

                relay.forwardMessage(destinationAddress, message);
            } catch (JiterSocketException e) {
                Log.getInstance().e("* Forwarding failed:");
                Log.getInstance().e(Utils.getStackTraceString(e));
            }

        }

        return true;
    }

    public boolean handleSendMessage(JiterSocket socket, Address destinationAddress, JiterMessage message) {
        return false;
    }
}
