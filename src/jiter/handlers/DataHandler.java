package jiter.handlers;

import org.jgroups.Address;

import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocket;
import jiter.misc.Log;

public class DataHandler implements IMessageHandler {
    public boolean handleReceiveMessage(JiterSocket socket, JiterMessage message) {
        JiterNode node = socket.getNode();
        Address destinationAddress = node.getAddressFromName(message.getDestinationName());

        // if the node is not a relay of the channel
        if ((destinationAddress == null || node.isLocalSocketAddress(destinationAddress) || node.isTestSocketAddress(destinationAddress)) &&
            message.getType() == JiterMessage.Type.DATA) {
            //System.out.println("Received DATA message:");
            //System.out.println(message.toString());
        	Log.getInstance().d("DataHandler: Notifying node of received data.");
            node.notifyDataReceived(message);
        }
        return false;
    }

    public boolean handleSendMessage(JiterSocket socket, Address destinationAddress, JiterMessage message) {
        return false;
    }
}
