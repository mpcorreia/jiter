package jiter.handlers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;

import org.jgroups.Address;

import jiter.DirectChannelMatrix;
import jiter.JiterDirectChannel;
import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocket;
import jiter.misc.Log;
import jiter.misc.Utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class DCHandler implements IMessageHandler, Observer {
    private static final int dcMatrixUpdateInterval = 60000; // 1 min

    private JiterNode _node;
    private DirectChannelMatrix _dcMatrix;
    private Timer _dcMatrixUpdateTimer;
    private boolean _waitingForMoreChanges;

    public DCHandler(JiterNode node) {
        _node = node;
        _dcMatrix = node.getDirectChannelMatrix();
        _dcMatrix.addObserver(this);
        _dcMatrixUpdateTimer = new Timer(true);
        _waitingForMoreChanges = false;
    }

    public void update(Observable o, Object arg) {
        if (!_waitingForMoreChanges) {
            _dcMatrixUpdateTimer.schedule(this.new DCMatrixUpdateSender(), dcMatrixUpdateInterval);
            _waitingForMoreChanges = true;
        }
    }

    public boolean handleReceiveMessage(JiterSocket socket, JiterMessage message) {
        JiterMessage.Type messageType = message.getType();

        // If received message is not a DCSYNC message we have nothing to do here...
        if (messageType != JiterMessage.Type.DCSYNC) {
            return false;
        }

        BASE64Decoder b64d = new BASE64Decoder();
        
        try {
            byte [] data = b64d.decodeBuffer(message.getData());
            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
            @SuppressWarnings("unchecked")
            Collection<JiterDirectChannel> o = (Collection<JiterDirectChannel>) ois.readObject();
            ois.close();

            _dcMatrix.updateWithExternalData(new ArrayList<JiterDirectChannel>(o));
            
            //System.out.println("RECEIVED: " + o.toString());
        } catch (Exception e1) {
            Log.getInstance().e("DC matrix update failed");
            Log.getInstance().e(Utils.getStackTraceString(e1));
        } 

        return false;
    }

    public boolean handleSendMessage(JiterSocket socket, Address destinationAddress, JiterMessage message) {
        return false;
    }

    private class DCMatrixUpdateSender extends TimerTask {
		@Override
		public void run() {
			Set<String> nodeIds = new TreeSet<String>();
			
			for(JiterDirectChannel channel : _dcMatrix.getAllChannels()) {
                if (_node.getId().equals(channel.getDestinationNodeId())) {
                    continue;
                }

				if (!nodeIds.contains(channel.getDestinationNodeId())) {
					JiterSocket socket = _node.getLocalSocket(channel.getSource());
					if (socket != null) {
                        nodeIds.add(channel.getDestinationNodeId());
						doDCMatrixUpdate(socket, channel);
					} else {
						continue;
					}
				} else {
					//Do nothing -> update already sent
				}
			}
			
            _waitingForMoreChanges = false;
		}

        private void doDCMatrixUpdate(JiterSocket socket, JiterDirectChannel channel) {
            JiterMessage message = new JiterMessage();
            message.setSourceAddress(channel.getSource());
            message.setDestinationAddress(channel.getDestination());
            message.setType(JiterMessage.Type.DCSYNC);
            
            try {
                BASE64Encoder b64e = new BASE64Encoder();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream( baos );
                oos.writeObject( _node.getDirectChannelMatrix().getChannelsWithSourceNode(_node.getId()) );
                oos.close();
                
                message.setData(new String(b64e.encode(baos.toByteArray())));
                
                socket.sendMessage(channel.getDestination(), message);
                
                Log.getInstance().d("Sent DCMatrix: " + _node.getDirectChannelMatrix().getChannelsWithSourceNode(_node.getId()));
                
            } catch (Exception e) {
                Log.getInstance().e("Couldn't send the DCMatrixUpdate message from node: "
                        + channel.getSourceNodeId() + " to node: " + channel.getDestinationNodeId());
                Log.getInstance().e(Utils.getStackTraceString(e));
            }
        }
    }
}
