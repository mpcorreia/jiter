package jiter.handlers;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import org.jgroups.Address;

import jiter.JiterDirectChannel;
import jiter.JiterMessage;
import jiter.JiterNode;
import jiter.JiterSocket;
import jiter.JiterSocketException;

import jiter.misc.Config;
import jiter.misc.Log;
import jiter.misc.Utils;

public class AckHandler extends Observable implements IMessageHandler {

    /** Set of messages that are still to be Acked. */
    private Map<Long, UnackedMessageData> _unackedMessages;

    /** Timer to schedule the wait for ACKs. */
    private Timer _timer;

    private List<IAckObserver> _registeredObservers;

    public AckHandler() {
        _unackedMessages = new HashMap<Long, UnackedMessageData>();
        _timer = new Timer(true);
        _registeredObservers = new LinkedList<IAckObserver>();
    }

    public void registerObserver(IAckObserver observer) {
        synchronized(_registeredObservers) {
            _registeredObservers.add(observer);
        }
    }

    public void unregisterObserver(IAckObserver observer) {
        synchronized(_registeredObservers) {
            _registeredObservers.remove(observer);
        }
    }

    public boolean handleReceiveMessage(JiterSocket socket, JiterMessage message) {
        JiterNode node = socket.getNode();
        Address destinationAddress = node.getAddressFromName(message.getDestinationName());
        JiterMessage.Type messageType = message.getType();

        // If the destination of this message is unknown, we can't do anything
        if (destinationAddress == null) {
 	    Log.getInstance().d("ACK Handler: Got a message with destination null (mpc)");  //mpc
            return false;
        }

        if (messageType != JiterMessage.Type.ACK) {
            try {
                JiterMessage ackMessage = message.getAckMessage();
                Address destination = node.getAddressFromName(ackMessage.getDestinationName());
                if (destination != null) {
                    try {
                        //Thread.sleep(3000);
                    } catch (Exception e) {
                    }
                    socket.sendMessage(destination, ackMessage);
                }
            } catch (JiterSocketException e) {
    		Log.getInstance().i("ACK Handler: Failed to send ack message to " + Utils.addressToLogicalName(destinationAddress));
                Log.getInstance().e(e.getMessage());
            	Log.getInstance().e(Utils.getStackTraceString(e));
            }
        } else {
            long ackId = message.getAckId();

            Log.getInstance().d("ACK Handler: Received ack for message: " + ackId);
            UnackedMessageData umd = null;

            synchronized(_unackedMessages) {
                umd = getUnackedMessage(ackId);

                if (umd == null) {
                    return false;
                }

                removeUnackedMessage(umd);
                if (umd.getOriginalMessage().getMessageId() == Config.getInstance().getExperimentMessageId()) {
                	Log.getInstance().setAckTime(Calendar.getInstance().getTimeInMillis() - umd.getOriginalMessageSendDate().getTimeInMillis());
                }
            }

            updateChannelTxt(umd);

            synchronized(_registeredObservers) {
                for (IAckObserver observer : _registeredObservers) {
                    observer.sendSuccessful(umd);

                }
            }
        }

        return false;
    }

    public boolean handleSendMessage(JiterSocket socket, Address destinationAddress, JiterMessage message) {
        if (message.getType() != JiterMessage.Type.ACK) {
            addUnackedMessage(socket, destinationAddress, message);
        }

        return false;
    }

    public boolean isMessageUnacked(long messageId) {
        return getUnackedMessage(messageId) != null;
    }

    private UnackedMessageData getUnackedMessage(long messageId) {
        synchronized(_unackedMessages) {
            return _unackedMessages.get(messageId);
        }
    }

    private void addUnackedMessage(JiterSocket socket, Address destinationAddress, JiterMessage message) {
        synchronized(_unackedMessages) {
            UnackedMessageData existingMessage = getUnackedMessage(message.getMessageId());
            if (existingMessage != null) {
                existingMessage.close();
            }
            _unackedMessages.put(message.getMessageId(), this.new UnackedMessageData(socket, destinationAddress, message));
        }
    }

    private void removeUnackedMessage(UnackedMessageData umd) {
        synchronized(_unackedMessages) {
            if (umd != null) {
                umd.close();
                _unackedMessages.remove(umd.getOriginalMessage().getMessageId());
            }
        }
    }

    private void notifySendFailed(UnackedMessageData umd) {
        Log.getInstance().e("ACK Handler: Failed to send the following message to " + umd.getDestinationAddress());
        Log.getInstance().e(umd.getOriginalMessage().toString());

        for (IAckObserver observer : _registeredObservers) {
            observer.sendFailed(umd);
        }
    }

    private void updateChannelTxt(UnackedMessageData umd) {
        assert umd != null : "umd cannot be null";

    	JiterMessage originalMessage = umd.getOriginalMessage();
        String destinationName = Utils.addressToLogicalName(umd.getDestinationAddress());
        String realDestinationName = umd.getOriginalMessage().getDestinationName();

        if (destinationName == null) 
            return;
    	
    	if (destinationName.equals(realDestinationName)) {
        	long actualMilliSeconds = Calendar.getInstance().getTimeInMillis();
        	long originalSendMilliSeconds = umd.getOriginalMessageSendDate().getTimeInMillis();
        	long rtt = actualMilliSeconds - originalSendMilliSeconds;
        	double txt = (rtt / 2);

            JiterSocket socket = umd.getSocket();
            JiterNode node = socket.getNode();
        	JiterDirectChannel channel = node.getJiterDirectChannel(originalMessage.getSourceName(), destinationName);
        	
        	if (channel != null) {
        		channel.updateChannelTxt(txt);
        	}
    	}
    }

    /**
     * This class handles the unacked messages and respective timeouts.
     * 
     * @author Alexandre Fonseca
	 * @author Pedro Marques da Luz
	 * @author Rui Silva
	 * @version 1.0
     */
    public class UnackedMessageData {
        private JiterSocket _socket;
    	
    	/** Destination Address of the unacked message. */
        private Address _destinationAddress;
        
        /** The unacked message. */
        private JiterMessage _message;
        
        /** Task to handle the timeout of the unacked message. */
        private TimeoutHandlerTask _timeoutHandler;
        
        /** Date when the original message was sent. */
        private Calendar _originalMessageSendDate;
        
        /**
         * Creates an instance of an UnackedMessageData.
         * 
         * @param destinationAddress The Address of the destination of the unacked message.
         * @param message The unacked message.
         */
        public UnackedMessageData(JiterSocket socket, Address destinationAddress, JiterMessage message) {
            _socket = socket;
            _destinationAddress = destinationAddress;
            _message = message;
            _timeoutHandler = this.new TimeoutHandlerTask();
            int ackTimeoutInterval = Config.getInstance().getAckTimeoutInterval();
            if (ackTimeoutInterval > 0) {
                _timer.schedule(_timeoutHandler, ackTimeoutInterval);
            }
            _originalMessageSendDate = Calendar.getInstance();
        }

        public JiterSocket getSocket() {
            return _socket;
        }
        
        /**
         * Returns the date when the original message was sent.
         * 
         * @return The date when the original message was sent.
         */
        public Calendar getOriginalMessageSendDate() {
        	return _originalMessageSendDate;
        }
        
        public JiterMessage getOriginalMessage() {
        	return _message;
        }

        public Address getDestinationAddress() {
            return _destinationAddress;
        }

        /**
         * Stops the timeout handler.
         * Should be called when an unacked message gets acked.
         */
        public void close() {
            if (_timeoutHandler != null) {
                _timeoutHandler.cancel();
            }
        }

        /**
         * This class handles the the timeout task.
         * 
         * @author Alexandre Fonseca
		 * @author Pedro Marques da Luz
		 * @author Rui Silva
		 * @version 1.0
         */
        private class TimeoutHandlerTask extends TimerTask {
        	
        	/**
        	 * Handles the timeout task.
        	 */
            public void run() {
                notifySendFailed(UnackedMessageData.this);
            }
        }
    }

    public interface IAckObserver {
        public void sendFailed(UnackedMessageData umd);
        public void sendSuccessful(UnackedMessageData umd);
    }
}
