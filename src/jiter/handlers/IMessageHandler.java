package jiter.handlers;

import org.jgroups.Address;

import jiter.JiterMessage;
import jiter.JiterSocket;

public interface IMessageHandler {
    public boolean handleReceiveMessage(JiterSocket socket, JiterMessage message);
    public boolean handleSendMessage(JiterSocket socket, Address destinationAddress, JiterMessage message);
}
