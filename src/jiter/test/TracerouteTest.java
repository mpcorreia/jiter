package jiter.test;

import jiter.misc.Traceroute;

public class TracerouteTest {
    public static void main(String[] args) {
        try {
            Traceroute.Node[] nodes = Traceroute.run(args[0], args[1]);

            for (Traceroute.Node node : nodes) {
                System.out.println(node.getIp().getHostAddress() + " - " + node.getRtt());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
