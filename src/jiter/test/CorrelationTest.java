package jiter.test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * This test tests the correlation function from JPaperStrategy.
 * In order to run this test the function must be declared as static.
 * The final result of this correlation test is 0.4.
 * 
 * The line were the result is calculated is commented in order to
 * allow the project to be compiled even if the correlation function
 * is not static.
 */

public class CorrelationTest {
	public static void main(String[] args) {
       List<InetAddress> base = new ArrayList<InetAddress>();
       List<InetAddress> channel = new ArrayList<InetAddress>();
       
       	try {
			base.add(InetAddress.getByName("123.123.123.120"));
			base.add(InetAddress.getByName("123.123.123.121"));
			base.add(InetAddress.getByName("123.123.123.122"));
			base.add(InetAddress.getByName("123.123.123.123"));
			base.add(InetAddress.getByName("123.123.123.124"));
			base.add(InetAddress.getByName("123.123.123.125"));
			base.add(InetAddress.getByName("123.123.123.126"));
			base.add(InetAddress.getByName("123.123.123.127"));
			base.add(InetAddress.getByName("123.123.123.128"));
			base.add(InetAddress.getByName("123.123.123.129"));
			
			channel.add(InetAddress.getByName("123.123.123.123"));
			channel.add(InetAddress.getByName("123.123.123.123"));
			channel.add(InetAddress.getByName("123.123.123.123"));
			channel.add(InetAddress.getByName("122.123.123.153"));
			channel.add(InetAddress.getByName("122.123.123.153"));
			channel.add(InetAddress.getByName("123.123.123.123"));
			channel.add(InetAddress.getByName("122.123.123.153"));
			channel.add(InetAddress.getByName("122.123.123.153"));
			channel.add(InetAddress.getByName("122.123.123.153"));
			channel.add(InetAddress.getByName("122.123.123.153"));
			
			//System.out.println(JPaperStrategy.correlation(base, channel));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
    }
}
