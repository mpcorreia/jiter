package jiter.test;

import java.net.InetAddress;
import java.util.List;

import org.jgroups.Address;

import jiter.IJiterChannel;
import jiter.JiterDirectChannel;
import jiter.misc.Log;
import jiter.misc.Utils;

public class LogTest {

	public static void main(String[] args) {
		
		Log l = Log.getInstance();
		
		l.startNewExperiment("testSource", "testDestination", 10, "j0");
		
		l.d("Test: d message");
		l.e("Test: e message");
		l.i("Test: i message");
		
		l.countNewMessage();
		l.countNewMessage();
		l.setAckTime(20);
		l.addNewJiterChannelUsed(new IJiterChannel() {
			
			@Override
			public String toDetailedString() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public float getTxt() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public String getSourceNodeId() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Address getSource() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List<InetAddress> getRoute() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getRelayNodeId() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Address getRelay() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List<String> getIsps() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getDestinationNodeId() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Address getDestination() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public String toString() {
				return "[JiterDirectChannel " + "source" + " | " + "destination" + "]";
			}
		});
		l.addNewJiterChannelUsed(new IJiterChannel() {
			
			@Override
			public String toDetailedString() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public float getTxt() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public String getSourceNodeId() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Address getSource() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List<InetAddress> getRoute() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getRelayNodeId() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Address getRelay() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List<String> getIsps() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getDestinationNodeId() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Address getDestination() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public String toString() {
				return "[JiterDirectChannel " + "source" + " | " + "destination" + "]";
			}
		});
		l.stopExperiment(0);
		l.stopLog();
	}
	
}
